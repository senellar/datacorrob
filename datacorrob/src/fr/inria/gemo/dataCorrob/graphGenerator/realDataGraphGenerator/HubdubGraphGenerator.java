/**
 * QCM1GraphGenerator.java in dataCorrob
 * Copyright (C) by INRIA-Saclay, Alban Galland, 12 nov. 2008, All rights reserved
 */
package fr.inria.gemo.dataCorrob.graphGenerator.realDataGraphGenerator;

import java.util.Iterator;
import java.util.Vector;

import fr.inria.gemo.dataCorrob.arguments.Arguments;

/**
 * This class implements a graph generator for hubdub data set
 */
public class HubdubGraphGenerator extends RealDataGraphGenerator {

	public HubdubGraphGenerator(Arguments args) {
		super(args);
	}

	@Override
	public String getFile() {
		return "data/hubdub.xml";
	}

	@Override
	public Iterator<String> getSourceNames() {
		Vector <String> names = new Vector <String> ();
		for(int i=0;i<473;i++) {
			if(i==286 || i==351) {
				i++;
			}
			names.add("u"+i);
		}
		return names.iterator();
	}
}
