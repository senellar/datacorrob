/**
 * GradientTwoStepsMethod.java in dataCorrob
 * Copyright (C) by INRIA-Saclay, Alban Galland, 17 nov. 2008, All rights reserved
 */
package fr.inria.gemo.dataCorrob.method.probabilityMethod;

import java.util.Iterator;

import fr.inria.gemo.dataCorrob.arguments.Arguments;
import fr.inria.gemo.dataCorrob.data.Link;
import fr.inria.gemo.dataCorrob.dataManager.GraphManager;

/**
 * This class implements 
 */
public class GradientTwoStepsMethod extends TwoStepsMethod {

	private static double delta = 0.001;

	private static double step = 1;

	/**
	 * This function is the standard constructor of the class.
	 * @param gm the graph manager
	 * @param args the arguments of the program
	 */
	public GradientTwoStepsMethod(GraphManager gm, Arguments args) {
		super(gm, args);
	}

	protected void computeAlphaFact(int f) {
		double logPd = 0;
		double logP = 0;
		double af = this.alpha.getValue(f);
		double afd = af+GradientTwoStepsMethod.delta;
		Iterator <Link> links = this.gm.getLinks(f);
		while(links.hasNext()) {
			Link l = links.next();
			double es = this.epsilon.getValue(l.getTo());
			if(es<=0) {
				es = GradientTwoStepsMethod.delta;
			} else if (es>=1) {
				es = 1 - GradientTwoStepsMethod.delta;
			}
			if(l.isPos()) {
				logP += af*Math.log(es)+(1-af)*Math.log(1-es);
				logPd += afd*Math.log(es)+(1-afd)*Math.log(1-es);
			} else if (l.isNeg()) {
				logP += af*Math.log(1-es)+(1-af)*Math.log(es);
				logPd += afd*Math.log(1-es)+(1-afd)*Math.log(es);
			}
		}
		double alp;
		if(logPd==logP) {
			alp=af;
		} else {
			alp = af-logP/(logPd-logP)*GradientTwoStepsMethod.delta*GradientTwoStepsMethod.step;
		}
		this.alpha.setValue(f, alp);
	}

	protected void computeEpsilonSource(int s) {
		double logPd = 0;
		double logP = 0;
		double es = this.epsilon.getValue(s);
		if(es<=0) {
			es = GradientTwoStepsMethod.delta;
		} else if (es>=1-GradientTwoStepsMethod.delta) {
			es = 1 - 2*GradientTwoStepsMethod.delta;
		}
		double esd = es+GradientTwoStepsMethod.delta;
		Iterator <Link> links = this.gm.getLinks(s);
		while(links.hasNext()) {
			Link l = links.next();
			double af = this.alpha.getValue(l.getTo());
			if(l.isPos()) {
				logP += af*Math.log(es)+(1-af)*Math.log(1-es);
				logPd += af*Math.log(esd)+(1-af)*Math.log(1-esd);
			} else if (l.isNeg()) {
				logP += af*Math.log(1-es)+(1-af)*Math.log(es);
				logPd += af*Math.log(1-esd)+(1-af)*Math.log(esd);
			}
		}
		if(logPd==logP) {
			System.err.println("epsilon : logPd == logP");
		}
		double eps;
		if(logPd==logP) {
			eps = es;
		} else {
			eps = es-logP/(logPd-logP)*GradientTwoStepsMethod.delta*GradientTwoStepsMethod.step;
		}
		this.epsilon.setValue(s, eps);
	}
}
