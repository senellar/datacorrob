/**
 * SimpleGraphGenerator.java in dataCorrob (C) 25 juil. 08 by agalland
 */
package fr.inria.gemo.dataCorrob.graphGenerator.syntheticGraphGenerator;

import fr.inria.gemo.dataCorrob.arguments.Arguments;

/**
 * This class implements a simple graph generator with one class without
 * negative links
 * @author agalland (Alban Galland) for Inria Saclay
 */
public class PosGraphGenerator extends PosNegGraphGenerator {
	/**
	 * This function is the standard constructor of the class.
	 * @param args the arguments of the program
	 */
	public PosGraphGenerator(Arguments args) {
		super(args);
		this.args.FactPhi.pNeg = 1;
		this.args.FactPhi.dpNeg = 0;
		this.args.SourcePhi.pNeg = 1;
		this.args.SourcePhi.dpNeg = 0;
	}
}
