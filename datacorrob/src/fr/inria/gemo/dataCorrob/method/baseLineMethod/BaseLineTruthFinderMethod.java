/**
 * BaseLinetruthFinder.java in dataCorrob (C) 28 jul. 2009 by agalland 
 */
package fr.inria.gemo.dataCorrob.method.baseLineMethod;

import java.util.Iterator;

import fr.inria.gemo.dataCorrob.arguments.Arguments;
import fr.inria.gemo.dataCorrob.data.Link;
import fr.inria.gemo.dataCorrob.dataManager.AnnotationManager;
import fr.inria.gemo.dataCorrob.dataManager.GraphManager;
import fr.inria.gemo.dataCorrob.method.Method;

/**
 * This class implements the truth finder algorithm
 * @author agalland (Alban Galland) for INRIA Saclay
 */
public class BaseLineTruthFinderMethod extends Method {

	public static double t0=0.9;
	
	public static double rho=0.5;
	
	public static double gamma = 0.3; 

	protected AnnotationManager score;
	
	public BaseLineTruthFinderMethod(GraphManager gm, Arguments args) {
		super(gm, args);
		this.score = AnnotationManager.create(args);
	}

	@Override
	protected double getSimpleScore(int id) {
		if(this.gm.getIsFact(id)) {
			return 2*this.score.getValue(id)-1;
		} else {
			return this.score.getValue(id);
		}
	}

	@Override
	public void init() {
		Iterator <Integer> nodes = gm.getSources();
		while(nodes.hasNext()) {
			this.score.setValue(nodes.next(),0);
		}
		nodes = gm.getFacts();
		while(nodes.hasNext()) {
			this.score.setValue(nodes.next(),BaseLineTruthFinderMethod.t0);
		}
	}

	@Override
	protected void iterate() {
		Iterator <Integer> nodes = gm.getSources();
		while(nodes.hasNext()) {
			Integer n = nodes.next();
			double sum=0;
			double npos = 0;
			Iterator <Link> links = this.gm.getLinks(n);
			while(links.hasNext()) {
				Link l = links.next();
				if(l.isPos()) {
					sum += this.score.getValue(l.getTo());
					npos++;
				}
			}
			if(npos>0) {
				this.score.setValue(n, sum/npos);
			} else {
				this.score.setValue(n, 0);
			}
		}
		nodes = gm.getFacts();
		while(nodes.hasNext()) {
		Integer n = nodes.next();
		double possum = 0;	
		Iterator<Link> links = this.gm.getLinks(n);
		while (links.hasNext()) {
		   Link l = links.next();
		   if (l.isPos()) {
			 possum -= Math.log(1-this.score.getValue(l.getTo()));
		   } 
		}
		double negsum = 0;
		String q = this.gm.getQueryId(n);
		Iterator <Integer> facts = gm.getFacts();
		while(facts.hasNext()) {
			Integer f = facts.next();
			if(q.equals(this.gm.getQueryId(f))) {
				negsum += this.score.getValue(f);
			}
		}
		double sigma = possum - BaseLineTruthFinderMethod.rho*negsum;
		double s = 1.0/(1+Math.exp(-BaseLineTruthFinderMethod.gamma*sigma));
		this.score.setValue(n, s);
		}
	}
}
