/**
 * GraphGenerationTest.java in dataCorrob (C) august 08 by agalland
 */
package fr.inria.gemo.dataCorrob.test.graphTest;

import java.util.Iterator;

import fr.inria.gemo.dataCorrob.arguments.Arguments;
import fr.inria.gemo.dataCorrob.data.Link;
import fr.inria.gemo.dataCorrob.dataManager.GraphManager;
import fr.inria.gemo.dataCorrob.graphGenerator.syntheticGraphGenerator.PosNegGraphGenerator;
import fr.inria.gemo.dataCorrob.method.Method;

/**
 * This class implements a test which plots the distribution of the parameters
 * of the graph
 * @author agalland (Alban Galland) for Inria Saclay
 */
public class PlotGraphGenerationTest extends GraphTest {

	private static int nbPoints=16;

	/**
	 * This function is the standard constructor of the class.
	 * @param args the arguments of the program
	 */
	public PlotGraphGenerationTest(Arguments args) {
		super(args,"Plot of the arguments of the generation");
	}

	@Override
	protected void runTest(GraphManager gm) {
		double nbLinks = 0;
		double nbFacts = gm.getNbFacts();
		double nbSources = gm.getNbSources();
		//init distributions
		int[] distFacts = new int[PlotGraphGenerationTest.nbPoints];
		int[] distLinks = new int[PlotGraphGenerationTest.nbPoints];
		int[] distFactProb = new int[PlotGraphGenerationTest.nbPoints];
		int[] distSourceProb = new int[PlotGraphGenerationTest.nbPoints];
		for (int i = 0; i < PlotGraphGenerationTest.nbPoints; i++) {
			distFacts[i] = 0;
			distLinks[i] = 0;
			distFactProb[i] = 0;
			distSourceProb[i] = 0;
		}
		//Computes distribution facts
		Iterator <Integer> facts = gm.getFacts();
		while (facts.hasNext()) {
			int f = facts.next();
			distFactProb[this.toIndex(gm.getPError(f))]++;
			if(gm.getIsTrue(f)) {
				distFacts[PlotGraphGenerationTest.nbPoints-1]++;
			} else {
				distFacts[0]++;	
			}
			Iterator <Link> ls = gm.getLinks(f);
			while(ls.hasNext()) {
				Link l = ls.next();
				nbLinks++;
				if(l.isPos()) {
					distLinks[PlotGraphGenerationTest.nbPoints-1]++;
				} else if (l.isNeg()){
					distLinks[0]++;
				}
			}
		}
		//Computes distribution sources
		Iterator <Integer> sources = gm.getSources();
		while (sources.hasNext()) {
			int s = sources.next();
			distSourceProb[this.toIndex(gm.getPError(s))]++;
		}

		double size = 1/((double) PlotGraphGenerationTest.nbPoints-1);
		if (this.args.point) {
			//Plot points
			for (int i = 0; i < PlotGraphGenerationTest.nbPoints; i++) {
				this.plotPoint(i*size, distFacts[i] / nbFacts,
				"black");
				this.plotPoint(i*size, distLinks[i] / nbLinks,
				"grey");
				this.plotPoint(i*size, distFactProb[i] / nbFacts,
				"blue");
				this.plotPoint(i*size, distSourceProb[i] / nbSources,
				"cyan");
			}
		}
		if (this.args.line) {
			//Plot lines
			for (int i = 0; i < PlotGraphGenerationTest.nbPoints-1; i++) {
				this.plotLine(i*size, (i + 1)*size, distFacts[i]
				                                              / nbFacts, distFacts[i + 1] / nbFacts, "black");
				this.plotLine(i*size, (i + 1)*size, distLinks[i]
				                                              / nbLinks, distLinks[i + 1] / nbLinks, "grey");
				this.plotLine(i*size, (i + 1)*size, distFactProb[i]
				                                                 / nbFacts, distFactProb[i + 1] / nbFacts, "blue");
				this.plotLine(i*size, (i + 1)*size,
						distSourceProb[i] / nbSources, distSourceProb[i + 1]
						                                              / nbSources, "cyan");
			}
		}
		System.out.println("GraphGenerationTest");
		System.out.println("Facts [black]");
		System.out.println("links [grey]");
		System.out.println("Fact Prob Error [blue]");
		System.out.println("Source Prob Error [cyan]");

		//if(false) {
		if(PosNegGraphGenerator.class.isAssignableFrom(args.typeGraphGenerator)) {
			//Computes average sources
			double sumPhiPosSources = 0;
			double sumPhiNegSources = 0;
			Iterator<Integer> nodes = gm.getSources();
			while (nodes.hasNext()) {
				int id = nodes.next();
				sumPhiPosSources+=gm.getPhiPos(id);
				sumPhiNegSources+=gm.getPhiNeg(id);
			}
			double avgPhiPosSources = sumPhiPosSources/nbSources;
			double avgPhiNegSources = sumPhiNegSources/nbSources;
			//Computes average facts
			double sumPhiPosFacts = 0;
			double sumPhiNegFacts = 0;
			nodes = gm.getFacts();
			while (nodes.hasNext()) {
				int id = nodes.next();
				sumPhiPosFacts+=gm.getPhiPos(id);
				sumPhiNegFacts+=gm.getPhiNeg(id);
			}
			double avgPhiPosFacts = sumPhiPosFacts/nbFacts; 
			double avgPhiNegFacts = sumPhiNegFacts/nbFacts; 
			//Initializes distribution
			int[] distFactForgPos = new int[PlotGraphGenerationTest.nbPoints];
			int[] distFactForgNeg = new int[PlotGraphGenerationTest.nbPoints];
			int[] distSourceForgPos = new int[PlotGraphGenerationTest.nbPoints];
			int[] distSourceForgNeg = new int[PlotGraphGenerationTest.nbPoints];
			for (int i = 0; i < PlotGraphGenerationTest.nbPoints; i++) {
				distFactForgNeg[i] = 0;
				distFactForgPos[i] = 0;
				distSourceForgNeg[i] = 0;
				distSourceForgPos[i] = 0;
			}
			//Computes distribution facts
			facts = gm.getFacts();
			while(facts.hasNext()) {
				int f = facts.next();
				distFactForgPos[this.toIndex(gm.getPhiPos(f)*avgPhiPosSources)]++;
				distFactForgNeg[this.toIndex(gm.getPhiNeg(f)*avgPhiNegSources)]++;
			}
			sources = gm.getSources();
			while(sources.hasNext()) {
				int s = sources.next();
				distSourceForgPos[this.toIndex(gm.getPhiPos(s)*avgPhiPosFacts)]++;
				distSourceForgNeg[this.toIndex(gm.getPhiNeg(s)*avgPhiNegFacts)]++;
			}
			if (this.args.point) {
				//Plot points
				for (int i = 0; i < PlotGraphGenerationTest.nbPoints; i++) {
					this.plotPoint(i*size, distFactForgPos[i] / nbFacts,
					"red");
					this.plotPoint(i*size, distFactForgNeg[i] / nbFacts,
					"orange");
					this.plotPoint(i*size, distSourceForgPos[i] / nbSources,
					"magenta");
					this.plotPoint(i*size, distSourceForgNeg[i] / nbSources,
					"yellow");
				}
			}
			if (this.args.line) {
				//Plot lines
				for (int i = 0; i < PlotGraphGenerationTest.nbPoints-1; i++) {
					this.plotLine(i*size, (i + 1)*size,
							distFactForgPos[i] / nbFacts, distFactForgPos[i + 1]
							                                              / nbFacts, "red");
					this.plotLine(i*size, (i + 1)*size,
							distFactForgNeg[i] / nbFacts, distFactForgNeg[i + 1]
							                                              / nbFacts, "orange");
					this.plotLine(i*size, (i + 1)*size,
							distSourceForgPos[i] / nbSources,
							distSourceForgPos[i + 1] / nbSources, "magenta");
					this.plotLine(i*size, (i + 1)*size,
							distSourceForgNeg[i] / nbSources,
							distSourceForgNeg[i + 1] / nbSources, "yellow");
				}
			}
			System.out.println("Fact Forget Pos [red]");
			System.out.println("Fact Forget Neg [orange]");
			System.out.println("Source Forget Pos [magenta]");
			System.out.println("Source Forget Neg [yellow]");
		}else {
			//Initializes distribution
			int[] distFactForgPos = new int[PlotGraphGenerationTest.nbPoints];
			int[] distFactForgNeg = new int[PlotGraphGenerationTest.nbPoints];
			int[] distSourceForgPos = new int[PlotGraphGenerationTest.nbPoints];
			int[] distSourceForgNeg = new int[PlotGraphGenerationTest.nbPoints];
			for (int i = 0; i < PlotGraphGenerationTest.nbPoints; i++) {
				distFactForgNeg[i] = 0;
				distFactForgPos[i] = 0;
				distSourceForgNeg[i] = 0;
				distSourceForgPos[i] = 0;
			}
			//Computes distribution facts
			facts = gm.getFacts();
			while(facts.hasNext()) {
				int f = facts.next();
				distFactForgPos[this.toIndex(gm.getPForgPos(f))]++;
				distFactForgNeg[this.toIndex(gm.getPForgNeg(f))]++;
			}
			sources = gm.getSources();
			while(sources.hasNext()) {
				int s = sources.next();
				distSourceForgPos[this.toIndex(gm.getPForgPos(s))]++;
				distSourceForgNeg[this.toIndex(gm.getPForgNeg(s))]++;
			}
			if (this.args.point) {
				//Plot points
				for (int i = 0; i < PlotGraphGenerationTest.nbPoints; i++) {
					this.plotPoint(i*size, distFactForgPos[i] / nbFacts,
					"red");
					this.plotPoint(i*size, distFactForgNeg[i] / nbFacts,
					"orange");
					this.plotPoint(i*size, distSourceForgPos[i] / nbSources,
					"magenta");
					this.plotPoint(i*size, distSourceForgNeg[i] / nbSources,
					"yellow");
				}
			}
			if (this.args.line) {
				//Plot lines
				for (int i = 0; i < PlotGraphGenerationTest.nbPoints-1; i++) {
					this.plotLine(i*size, (i + 1)*size,
							distFactForgPos[i] / nbFacts, distFactForgPos[i + 1]
							                                              / nbFacts, "red");
					this.plotLine(i*size, (i + 1)*size,
							distFactForgNeg[i] / nbFacts, distFactForgNeg[i + 1]
							                                              / nbFacts, "orange");
					this.plotLine(i*size, (i + 1)*size,
							distSourceForgPos[i] / nbSources,
							distSourceForgPos[i + 1] / nbSources, "magenta");
					this.plotLine(i*size, (i + 1)*size,
							distSourceForgNeg[i] / nbSources,
							distSourceForgNeg[i + 1] / nbSources, "yellow");
				}
			}
			System.out.println("Fact Prob Forget Pos [red]");
			System.out.println("Fact Prob Forget Neg [orange]");
			System.out.println("Source Prob Forget Pos [magenta]");
			System.out.println("Source Prob Forget Neg [yellow]");
		} 
		this.finishPlot();
	}


	private int toIndex(double d) {
		return Math.max(0, Math.min(PlotGraphGenerationTest.nbPoints-1, (int) Math.rint(d* PlotGraphGenerationTest.nbPoints)));
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.test.GraphTest#printBox(java.io.PrintStream)
	 */
	@Override
	protected void printBox() {
		this.BeginPlot(0, 0, 1, 1);
		this.plotVerticalSide(0,0,1);
		this.plotVerticalSide(1,0,1);
		this.plotHorizontalSide(0,1,0);
		this.plotHorizontalSide(0,1,1);
		this.plotDashLine(0.5, 0.5, 0, 1);
		this.plotText(-0.01, -0.02, "0");
		this.plotText(1.01, -0.02, "Probability");
		this.plotText(-0.01, 1.01, "% of nodes");
		this.plotText(1.01, 1.01, "1");
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.test.graphTest.GraphTest#plot(fr.inria.gemo.dataCorrob.dataManager.DataManager, fr.inria.gemo.dataCorrob.method.Method, java.lang.String, java.io.PrintStream)
	 */
	@Override
	protected void plot(GraphManager gm, Method m, String color) {
	}


	@Override
	protected String getFile() {
		return "out/GraphGeneration.xml";
	}

}
