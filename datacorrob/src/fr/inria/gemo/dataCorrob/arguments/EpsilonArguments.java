/**
 * PrecisionArguments.java in dataCorrob (C) august 08 by agalland
 */
package fr.inria.gemo.dataCorrob.arguments;

import org.w3c.dom.Node;

/**
 * This class implements a sub-part of the arguments used to run the project,
 * describing a sub-distribution of precision probabilities.
 * @author agalland (Alban Galland) for Inria Saclay
 */
public class EpsilonArguments {
	/**
	 * This field is the average precision probability of the sub-distribution
	 */
	public double p;
	/**
	 * This field is the delta of the precision probability of the
	 * sub-distribution
	 */
	public double dp;
	/**
	 * This field is the size of the sub-distribution (the rate on the total
	 * distribution)
	 */
	public double size;

	/**
	 * This function is the standard constructor of the class.
	 * @param p the average precision probability of the sub-distribution
	 * @param dp the delta of the precision probability of the sub-distribution
	 * @param size the size of the sub-distribution (the rate on the total
	 *            distribution)
	 */
	public EpsilonArguments(double p, double dp, double size) {
		this.p = p;
		this.dp = dp;
		this.size = size;
	}
	
	/**
	 * This function read the epsilon node type of the file
	 * @param n the precision node
	 * @return the corresponding arguments
	 */
	public static EpsilonArguments readEpsilon(Node n) {
		double p = new Double(n.getAttributes().getNamedItem("p")
				.getNodeValue());
		double dp = new Double(n.getAttributes().getNamedItem("dp")
				.getNodeValue());
		return new EpsilonArguments(p, dp, 1);
	}

	/**
	 * This function read the epsilonM node type of the file
	 * @param n the precision node
	 * @return the corresponding arguments
	 */
	public static EpsilonArguments readEpsilonM(Node n) {
		double p = new Double(n.getAttributes().getNamedItem("p")
				.getNodeValue());
		double dp = new Double(n.getAttributes().getNamedItem("dp")
				.getNodeValue());
		double size = new Double(n.getAttributes().getNamedItem("size")
				.getNodeValue());
		return new EpsilonArguments(p, dp, size);
	}
}
