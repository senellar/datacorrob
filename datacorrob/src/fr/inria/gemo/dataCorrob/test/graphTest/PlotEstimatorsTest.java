/**
 * PlotEstimatorsTest.java in dataCorrob (C) august 08 by agalland
 */
package fr.inria.gemo.dataCorrob.test.graphTest;

import java.util.Iterator;

import fr.inria.gemo.dataCorrob.arguments.Arguments;
import fr.inria.gemo.dataCorrob.dataManager.GraphManager;
import fr.inria.gemo.dataCorrob.method.Method;

/**
 * This class implements a test which plots the estimator given by the
 * propagator according to the real values
 * @author agalland (Alban Galland) for Inria Saclay
 */
public class PlotEstimatorsTest extends GraphTest {
	/**
	 * This function is the standard constructor of the class.
	 * @param args the arguments of the program
	 */
	public PlotEstimatorsTest(Arguments args) {
		super(args,"Plot of the estimators");
		this.sizePoint=5;
	}

	@Override
	protected void plot(GraphManager gm, Method m, String color) {
		Iterator<Integer> nodes = gm.getFacts();
		while (nodes.hasNext()) {
			int id = nodes.next();
				double score = m.getScore(id);
				double target = 0;
				if (gm.getIsTrue(id)) {
					target = 1-gm.getPError(id);
				} else {
					target = -(1-gm.getPError(id));
				}
				this.plotPoint(target, score, color);
		}
	}
	
	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.test.GraphTest#printBox(java.io.PrintStream)
	 */
	@Override
	protected void printBox() {
		this.BeginPlot(-1, -1, 1, 1);
		this.plotVerticalSide(-1,-1,1);
		this.plotVerticalSide(0,-1,1);
		this.plotVerticalSide(1,-1,1);
		this.plotHorizontalSide(-1,1,-1);
		this.plotHorizontalSide(-1,1,0);
		this.plotHorizontalSide(-1,1,1);
		this.plotDashLine(0.5, 0.5, -1, 1);
		this.plotDashLine(-0.5, -0.5, -1, 1);
		this.plotText(-1.01, -1.02, "-1");
		this.plotText(1.01, -0.02, "Probability of being believed correctly");
		this.plotText(-0.01, 1.01, "score");
		this.plotText(1.01, 1.01, "1");
	}
	
	
	@Override
	protected String getFile() {
		return "out/Estimators.xml";
	}
}
