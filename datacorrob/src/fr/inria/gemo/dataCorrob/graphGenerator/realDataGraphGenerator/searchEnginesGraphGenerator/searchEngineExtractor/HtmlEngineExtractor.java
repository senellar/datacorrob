/**
 * HtmlEngineExtractor.java in dataCorrob
 * Copyright (C) by INRIA-Saclay, Alban Galland, 27 oct. 2008, All rights reserved
 */
package fr.inria.gemo.dataCorrob.graphGenerator.realDataGraphGenerator.searchEnginesGraphGenerator.searchEngineExtractor;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * This class implements 
 */
public abstract class HtmlEngineExtractor extends SearchEngineExtractor {

	abstract public String getUrl(String q);

	abstract public String filtering(String s);

	@Override
	public Enumeration <Answer> runQuery(String q) {
		Hashtable <String,Answer> answers = new Hashtable <String,Answer>();
		Pattern p = Pattern.compile("\\s\\d{4}\\D");
		Matcher m = p.matcher(retrieveHTML(q));
		while(m.find()) {
			String a = m.group().substring(1,5);
			if(answers.containsKey(a)) {
				double weight = answers.get(a).weight + 1;
				answers.put(a, new Answer(a,weight));
			} else {
				answers.put(a, new Answer(a,1));
			}
		}
		return answers.elements();
	}

	private String retrieveHTML(String q) {
		String s = "";
		try {
			URL u = new URL(this.getUrl(q));
			URLConnection uc = u.openConnection();
			uc.setConnectTimeout(1000);
			uc.setReadTimeout(10000);
			uc.setRequestProperty("User-Agent","Mozilla/5.0 (Linux; X11; UTF-8)");
			InputStream is = uc.getInputStream();
			InputStreamReader isr = new InputStreamReader(is);
			BufferedReader br = new BufferedReader(isr);
			String t = "";
			while(t!=null) {
				t = br.readLine();
				s += t;
			}
			br.close();
			isr.close();
			is.close();
			try {
				Thread.sleep(3000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			s = this.filtering(s);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return s;
	}
}
