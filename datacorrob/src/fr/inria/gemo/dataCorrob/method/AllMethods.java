/**
 * AllMethod.java in dataCorrob (C) 25 sept. 08 by agalland 
 */
package fr.inria.gemo.dataCorrob.method;

import java.util.Vector;
import java.util.Iterator;

import fr.inria.gemo.dataCorrob.arguments.Arguments;
import fr.inria.gemo.dataCorrob.dataManager.GraphManager;
import fr.inria.gemo.dataCorrob.method.baseLineMethod.BaseLinePosMethod;
import fr.inria.gemo.dataCorrob.method.baseLineMethod.BaseLinePosNegMethod;
import fr.inria.gemo.dataCorrob.method.baseLineMethod.BaseLineTruthFinderMethod;
import fr.inria.gemo.dataCorrob.method.genericMethod.CosineSquareMethod;
import fr.inria.gemo.dataCorrob.method.probabilityMethod.ApproxThreeStepsMethod;
import fr.inria.gemo.dataCorrob.method.probabilityMethod.ApproxTwoStepsMethod;

/**
 * This class implements 
 * @author agalland (Alban Galland) for INRIA Saclay
 */
public class AllMethods extends Method {
	
	/**
	 * This static field is the list of methods executed by all methods
	 */
	public static Class<?> [] methods = {BaseLinePosMethod.class, BaseLinePosNegMethod.class, 
		BaseLineTruthFinderMethod.class, CosineSquareMethod.class,
		ApproxTwoStepsMethod.class, ApproxThreeStepsMethod.class};
	
	/**
	 * This static field is the internal way to store the methods for multi run.
	 */
	private Vector <Method> method;
	
	/**
	 * This function is the standard constructor of the class.
	 * @param gm the graph manager
	 * @param args the arguments of the program
	 */
	protected AllMethods(GraphManager gm,Arguments args) {
		super(gm,args);
		this.method = new Vector <Method>();
		for(int i=0;i<AllMethods.methods.length;i++) {
			this.method.add(Method.create(AllMethods.methods[i], args, gm));
		}
	}
	
	/**
	 * This function gives access to the sub-methods
	 * @return the list of the methods
	 */
	public Iterator <Method> getMethods() {
		return this.method.iterator();
	}

	/* (non-Javadoc)
	 * @see fr.inria.gforge.gemofusion.dataCorrob.method.Method#getScore(int)
	 */
	@Override
	public double getSimpleScore(int id) {
		System.err.println("Warning(AllMethods) : you are not expected to call this function (getScore)");
		return 0;
	}
	
	/* (non-Javadoc)
	 * @see fr.inria.gforge.gemofusion.dataCorrob.method.Method#init()
	 */
	@Override
	public void init() {
		Iterator <Method> ms = this.method.iterator();
		while(ms.hasNext()) {
			ms.next().init();
		}
	}

	/* (non-Javadoc)
	 * @see fr.inria.gforge.gemofusion.dataCorrob.method.Method#iterate()
	 */
	@Override
	protected void iterate() {}
	
	/*
	 * (non-Javadoc)
	 * @see fr.inria.gforge.gemofusion.dataCorrob.method.Method#iterate(int, boolean)
	 */
	@Override
	public void iterate(int nbIter) {
		Iterator <Method> ms = this.method.iterator();
		while(ms.hasNext()) {
			Method m = ms.next();
			m.iterate(nbIter);
		}
	}

}
