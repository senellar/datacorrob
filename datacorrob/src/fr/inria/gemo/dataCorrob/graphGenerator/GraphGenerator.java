/**
 * GraphGenerator.java in dataCorrob (C) 25 juil. 08 by agalland
 */
package fr.inria.gemo.dataCorrob.graphGenerator;

import java.util.Hashtable;
import java.util.Iterator;

import fr.inria.gemo.dataCorrob.arguments.Arguments;
import fr.inria.gemo.dataCorrob.data.Link;
import fr.inria.gemo.dataCorrob.dataManager.GraphManager;
import fr.inria.gemo.dataCorrob.graphGenerator.realDataGraphGenerator.QCM1GraphGenerator;
import fr.inria.gemo.dataCorrob.graphGenerator.realDataGraphGenerator.QCM2GraphGenerator;
import fr.inria.gemo.dataCorrob.graphGenerator.realDataGraphGenerator.searchEnginesGraphGenerator.BestAnswersSearchEnginesGraphGenerator;
import fr.inria.gemo.dataCorrob.graphGenerator.realDataGraphGenerator.searchEnginesGraphGenerator.SearchEnginesGraphGenerator;
import fr.inria.gemo.dataCorrob.graphGenerator.syntheticGraphGenerator.MultiClassPosGraphGenerator;
import fr.inria.gemo.dataCorrob.graphGenerator.syntheticGraphGenerator.MultiClassPosNegGraphGenerator;
import fr.inria.gemo.dataCorrob.graphGenerator.syntheticGraphGenerator.PosGraphGenerator;
import fr.inria.gemo.dataCorrob.graphGenerator.syntheticGraphGenerator.PosNegGraphGenerator;
import fr.inria.gemo.dataCorrob.graphGenerator.realDataGraphGenerator.HubdubGraphGenerator;

/**
 * This interface defines the generators of graphs
 * @author agalland (Alban Galland) for Inria Saclay
 */
public abstract class GraphGenerator {
	/**
	 * The number of true facts;
	 */
	private int nbTrueFacts;
	/**
	 * The number of errors
	 */
	private int nbErrors;
	/**
	 * The number of links which have been forget
	 */
	private int nbForgets;
	/**
	 * The number of links in the graph
	 */
	private int nbLinks;
	/**
	 * The number of positive links in the graph
	 */
	private int nbPosLinks;
	/**
	 * The number of negative links in the graph
	 */
	private int nbNegLinks;
	/**
	 * This field is the arguments of the program
	 */
	protected Arguments args;

	/**
	 * This function is the standard constructor of the class
	 * @param args the arguments of the program
	 */
	protected GraphGenerator(Arguments args) {
		this.args = args;
	}

	/**
	 * This function transforms a string to a sub-class of graph generator
	 * @param type the string denoting the class
	 * @return the corresponding class of graph-generator
	 */
	public static Class<?> toClass(String type) {
		Class<?> typeGraphGenerator = null;
		if (type.equalsIgnoreCase("Pos")) {
			typeGraphGenerator = PosGraphGenerator.class;
		} else if (type.equalsIgnoreCase("PosNeg")) {
			typeGraphGenerator = PosNegGraphGenerator.class;
		} else if (type.equalsIgnoreCase("MultiClassPos")) {
			typeGraphGenerator = MultiClassPosGraphGenerator.class;
		} else if (type.equalsIgnoreCase("MultiClassPosNeg")) {
			typeGraphGenerator = MultiClassPosNegGraphGenerator.class;
		} else if (type.equalsIgnoreCase("SearchEngines")) {
			typeGraphGenerator = SearchEnginesGraphGenerator.class;
		} else if (type.equalsIgnoreCase("BestAnswersSearchEngines")) {
			typeGraphGenerator = BestAnswersSearchEnginesGraphGenerator.class;
		} else if (type.equalsIgnoreCase("QCM1")) {
			typeGraphGenerator = QCM1GraphGenerator.class;
		} else if (type.equalsIgnoreCase("QCM2")) {
			typeGraphGenerator = QCM2GraphGenerator.class;
		} else if (type.equalsIgnoreCase("Hubdub")) {
			typeGraphGenerator = HubdubGraphGenerator.class;
		} else {
			System.err.println("Error (GraphGenerator.java): "+type+" is an unknown GraphGenerator type");
		}
		return typeGraphGenerator;
	}

	/**
	 * This function transforms a class to a string
	 * @param type the type of the graph generator
	 * @return the corresponding string
	 */
	public static String toString(Class <?> type) {
		String typeGraphGenerator = null;
		if (PosGraphGenerator.class.equals(type)) {
			typeGraphGenerator = "Pos";
		} else if (PosNegGraphGenerator.class.equals(type)) {
			typeGraphGenerator = "PosNeg";
		} else if (MultiClassPosGraphGenerator.class.equals(type)) {
			typeGraphGenerator = "MultiClassPos";
		} else if (MultiClassPosNegGraphGenerator.class.equals(type)) {
			typeGraphGenerator = "MultiClassPosNeg";
		} else if (SearchEnginesGraphGenerator.class.equals(type)) {
			typeGraphGenerator = "SearchEngines";
		} else if (BestAnswersSearchEnginesGraphGenerator.class.equals(type)) {
			typeGraphGenerator = "BestAnswersSearchEngines";
		} else if (QCM1GraphGenerator.class.equals(type)) {
			typeGraphGenerator = "QCM1";
		} else if (QCM2GraphGenerator.class.equals(type)) {
			typeGraphGenerator = "QCM2";
		} else if (HubdubGraphGenerator.class.equals(type)) {
			typeGraphGenerator = "Hubdub";
		} else {
			System.err.println("Error (GraphGenerator.java): unknown GraphGenerator type");
		}
		return typeGraphGenerator;
	}

	/**
	 * This function creates a graph generator of the specified class
	 * @param type the type of graph generator
	 * @param args the arguments of the program
	 * @return the corresponding object
	 */
	public static GraphGenerator create(Arguments args) {
		GraphGenerator g= null;
		if (PosGraphGenerator.class.equals(args.typeGraphGenerator)) {
			g = new PosGraphGenerator(args);
		} else if (PosNegGraphGenerator.class.equals(args.typeGraphGenerator)) {
			g = new PosNegGraphGenerator(args);
		} else if (MultiClassPosGraphGenerator.class.equals(args.typeGraphGenerator)) {
			g = new MultiClassPosGraphGenerator(args);
		} else if (MultiClassPosNegGraphGenerator.class.equals(args.typeGraphGenerator)) {
			g = new MultiClassPosNegGraphGenerator(args);
		} else if (SearchEnginesGraphGenerator.class.equals(args.typeGraphGenerator)) {
			g = new SearchEnginesGraphGenerator(args);
		} else if (BestAnswersSearchEnginesGraphGenerator.class.equals(args.typeGraphGenerator)) {
			g = new BestAnswersSearchEnginesGraphGenerator(args);
		} else if (QCM1GraphGenerator.class.equals(args.typeGraphGenerator)) {
			g = new QCM1GraphGenerator(args);
		} else if (QCM2GraphGenerator.class.equals(args.typeGraphGenerator)) {
			g = new QCM2GraphGenerator(args);
		} else if (HubdubGraphGenerator.class.equals(args.typeGraphGenerator)) {
			g = new HubdubGraphGenerator(args);
		} else {
			System.err.println("Error (GraphGenerator.java): unknown GraphGenerator type");
		}
		return g;
	}
	
	/**
	 * This function is the internal way to construct the graph manager
	 * @param gm the data manager to construct
	 */
	protected abstract void constructGraph(GraphManager gm);
	
	protected abstract void setStats(GraphManager gm);

	/**
	 * This function generates the graph from the arguments
	 * @return a data manager of the good type
	 */
	public GraphManager constructGraph() {
		this.nbTrueFacts = 0;
		this.nbErrors = 0;
		this.nbForgets = 0;
		this.nbLinks = 0;
		this.nbPosLinks=0;
		this.nbNegLinks=0;
		GraphManager gm = GraphManager.create(args);
		this.constructGraph(gm);
		if(this.args.dependencies) {
			this.completeGraph(gm);
		}
		this.setStats(gm);
		int nbSources = gm.getNbSources();
		Iterator <Integer> facts = gm.getFacts();
		while(facts.hasNext()) {
			int f = facts.next();
			int nError=0;
			int nplink=0;
			int nnlink=0;
			boolean isTrue = gm.getIsTrue(f);
			if(isTrue) {
				this.nbTrueFacts++;
			}
			Iterator <Link> ls = gm.getLinks(f);
			while(ls.hasNext()) {
				Link l = ls.next();
				if(l.isPos()) {
					nplink++;
					if(!isTrue) {
						nError++;
					}
				} else if (l.isNeg()) {
					nnlink++;
					if(isTrue) {
						nError++;
					}
				}
			}
			this.nbPosLinks += nplink;
			this.nbNegLinks += nnlink;
			this.nbErrors += nError;
			this.nbForgets += (nbSources-(nplink+nnlink));
		}
		this.nbLinks = this.nbPosLinks+this.nbNegLinks;
		System.out.println("New Graph (nbFacts=" + gm.getNbFacts()
				+ ", nbSources=" + gm.getNbSources() + ", nbTrueFacts= "+this.nbTrueFacts+", nbForgets="
				+ this.nbForgets + ", nbLinks=" + this.nbLinks + ", nbPosLinks=" + this.nbPosLinks + ", nbNegLinks=" + this.nbNegLinks + ", nbErrors="
				+ this.nbErrors +")");
		return gm;
	}
	
	/**
	 * This function complete the graph using functional dependencies
	 * @param gm the graph to be completed
	 */
	protected void completeGraph(GraphManager gm) {
		Iterator <Integer> ss = gm.getSources();
		while(ss.hasNext()) {
			int s = ss.next();
			Hashtable <Integer,Boolean> ls = new Hashtable <Integer,Boolean>();
			Hashtable <String,Integer> queries = new Hashtable <String,Integer>();
			Iterator <Link> links = gm.getLinks(s);
			while(links.hasNext()) {
				Link l = links.next();
				ls.put(l.getTo(), l.isPos());
				String query = gm.getQueryId(l.getTo());
				if (queries.containsKey(query)) {
					int i = queries.get(query)+1;
					queries.put(query, i);
				} else {
					queries.put(query, 1);
				}
			}
			Iterator <Integer> fs = gm.getFacts();
			while(fs.hasNext()) {
				int f = fs.next();
				String query = gm.getQueryId(f);
				if((!ls.containsKey(f)) && queries.containsKey(query)){
					if(queries.get(query)==1) {
						gm.addChildToNode(f, s, -1);
						gm.addChildToNode(s, f, -1);
					}
				}
			}
		}
	}
}
