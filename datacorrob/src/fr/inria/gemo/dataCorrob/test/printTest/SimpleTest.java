/**
 * SimpleTest.java in dataCorrob (C) august 08 by agalland
 */
package fr.inria.gemo.dataCorrob.test.printTest;

import java.util.Iterator;

import fr.inria.gemo.dataCorrob.arguments.Arguments;
import fr.inria.gemo.dataCorrob.dataManager.GraphManager;
import fr.inria.gemo.dataCorrob.method.Method;

/**
 * This class implements a simple test which print the graph and the score after
 * the run
 * @author agalland (Alban Galland) for Inria Saclay
 */
public class SimpleTest extends PrintTest {

	/**
	 * This field is the internal way to store the number of the current run
	 */
	private int nbRuns;

	/**
	 * This function is the standard constructor of the class.
	 * @param args the arguments of the program
	 */
	public SimpleTest(Arguments args) {
		super(args);
		this.nbRuns=0;
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.test.Test#print(fr.inria.gemo.dataCorrob.dataManager.DataManager, fr.inria.gemo.dataCorrob.method.Method)
	 */
	@Override
	protected String print(GraphManager gm, Method m) {
		String s="";
		Iterator <Integer> nodes = gm.getFacts();
		while (nodes.hasNext()) {
			int id = nodes.next();
			s+="Score=" + m.getScore(id) + " for fact "+id+"\n";
		}
		nodes = gm.getSources();
		while (nodes.hasNext()) {
			int id = nodes.next();
			s+="Score=" + m.getScore(id) + " for source "+id+"\n";
		}
		return s;
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.test.Test#printMulti()
	 */
	@Override
	protected void printMulti() {
		this.out.println("Warning(SimpleTest) : no way to aggregate results");
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.test.Test#save(fr.inria.gemo.dataCorrob.dataManager.DataManager, fr.inria.gemo.dataCorrob.method.Method)
	 */
	@Override
	protected void save(GraphManager gm, Method m) {
		this.nbRuns++;
		System.out.println("Run "+this.nbRuns+" : ");
		this.print(gm, m);
	}
}
