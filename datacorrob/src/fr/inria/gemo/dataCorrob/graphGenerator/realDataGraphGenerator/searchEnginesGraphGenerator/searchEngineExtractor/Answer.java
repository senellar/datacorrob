/**
 * Answer.java in dataCorrob
 * Copyright (C) by INRIA-Saclay, Alban Galland, 29 oct. 2008, All rights reserved
 */
package fr.inria.gemo.dataCorrob.graphGenerator.realDataGraphGenerator.searchEnginesGraphGenerator.searchEngineExtractor;

/**
 * This class implements 
 */
public class Answer {

	public String answer;
	
	public Double weight;
	
	public Answer(String answer,double weight) {
		this.answer=answer;
		this.weight=weight;
	}
	
}
