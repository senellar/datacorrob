/**
 * TwoStepsMethod.java in dataCorrob
 * Copyright (C) by INRIA-Saclay, Alban Galland, 17 nov. 2008, All rights reserved
 */
package fr.inria.gemo.dataCorrob.method.probabilityMethod;

import java.util.Iterator;

import fr.inria.gemo.dataCorrob.arguments.Arguments;
import fr.inria.gemo.dataCorrob.dataManager.AnnotationManager;
import fr.inria.gemo.dataCorrob.dataManager.GraphManager;
import fr.inria.gemo.dataCorrob.method.Method;

/**
 * This class implements 
 */
public abstract class TwoStepsMethod extends Method {

	protected AnnotationManager alpha;

	protected AnnotationManager epsilon;

	protected double nbIter;

	protected double normFactor;

	protected TwoStepsMethod(GraphManager gm, Arguments args) {
		super(gm, args);
		this.alpha = AnnotationManager.create(args);
		this.epsilon = AnnotationManager.create(args);
		this.normFactor = 0;
	}

	protected abstract void computeAlphaFact(int f);

	protected abstract void computeEpsilonSource(int s);

	@Override
	protected double getSimpleScore(int id) {
		if(this.gm.getIsFact(id)) {
			return (2*this.alpha.getValue(id) - 1);
		} else if (this.gm.getIsSource(id)) {
			return (1-this.epsilon.getValue(id));
		} else {
			return 0;
		}
	}

	@Override
	public void init() {
		this.nbIter=0;
		this.normFactor = 1;
		this.alpha.reset(0.5);
		this.epsilon.reset(0.1);
	}

	@Override
	protected void iterate() {
		this.iterateAlphaFacts();
		this.iterateEpsilonSources();
		this.nbIter++;
		if(this.nbIter<this.args.nbTotIter/2) {
			this.normFactor = 1-(2*this.nbIter/((double) this.args.nbTotIter));
		} else {
			this.normFactor=0;
		}
	}

	protected void iterateAlphaFacts() {
		Iterator <Integer> ids = this.gm.getFacts();
		while(ids.hasNext()) {
			this.computeAlphaFact(ids.next());
		}
		this.normalizeAlphaFacts();
	}

	protected void iterateEpsilonSources() {
		Iterator <Integer> ids = this.gm.getSources();
		while(ids.hasNext()) {
			this.computeEpsilonSource(ids.next());
		}
		this.normalizeEpsilonSources();
	}

	protected void normalizeAlphaFacts() {
		double maxaf=0;
		double minaf=1;
		Iterator <Integer> ids = this.gm.getFacts();
		while(ids.hasNext()) {
			Integer id = ids.next();
			double af = this.alpha.getValue(id); 
			if(af>maxaf) {
				maxaf=af;
			}
			if(af<minaf) {
				minaf=af;
			}
		}
		ids = this.gm.getFacts();
		while(ids.hasNext()) {
			int f = ids.next();
			double af = this.alpha.getValue(f);
			double normaf1;
			if(maxaf!=minaf) {
				normaf1 = (af-minaf)/(maxaf-minaf);
			} else {
				normaf1=af;
			}
			double normaf2;
			if(maxaf-minaf>1) {
				normaf2 = normaf1;
			} else if (maxaf>1) {
				normaf2 = af+(1-maxaf);
				//another possibility normaf2 = af/maxaf;
			} else if (minaf<0) {
				normaf2 = af-minaf;
				//another possibility normaf2 = 1-(1-af)/(1-minaf);
			} else {
				normaf2=af;
			}
			if(normaf2<0.5) {
				normaf2=0;
			} else if (normaf2>0.5){
				normaf2=1;
			} else {
				normaf2=1; //could be 0.5 or 0 too
			}
			double mergedaf = this.normFactor*normaf1+(1-this.normFactor)*normaf2;
			this.alpha.setValue(f, mergedaf);
		}
	}

	protected void normalizeEpsilonSources(){
		double maxEpsilon=0;
		double minEpsilon=1;
		Iterator <Integer> ids = this.gm.getSources();
		while(ids.hasNext()) {
			Integer id = ids.next();
			double es = this.epsilon.getValue(id); 
			if(es>maxEpsilon) {
				maxEpsilon = es;
			}
			if(es<minEpsilon) {
				minEpsilon = es;
			}
		}

		ids = this.gm.getSources();
		while(ids.hasNext()) {
			Integer id = ids.next();
			double value = this.epsilon.getValue(id);
			double normedValue1;
			if(maxEpsilon!=minEpsilon) {
				normedValue1 = (value-minEpsilon)/(maxEpsilon-minEpsilon);
			} else {
				normedValue1=value;
			}
			double normedValue2;
			if(maxEpsilon-minEpsilon>1) {
				normedValue2 = normedValue1;	
			} else if (maxEpsilon>1) {
				normedValue2 = value+(1-maxEpsilon);
				//another possibility normedValue2 = value/maxEpsilon;
			} else if (minEpsilon<0){
				normedValue2 = value-minEpsilon;
				//another possibility normedValue2 = 1-(1-value)/(1-minEpsilon);
			} else {
				normedValue2 = value;
			}
			double mergedValue = this.normFactor * normedValue1 + (1-this.normFactor) * normedValue2;
			this.epsilon.setValue(id, mergedValue);
		}
	}
	
	protected void optimistModel() {
		double sumEpsilon=0;
		Iterator <Integer> ids = this.gm.getSources();
		while(ids.hasNext()) {
			Integer id = ids.next();
			sumEpsilon += this.epsilon.getValue(id); 			
		}
		double avgEpsilon = sumEpsilon/gm.getNbSources();
		if(avgEpsilon>0.5) {
			ids = this.gm.getSources();
			while(ids.hasNext()) {
				Integer id = ids.next();
				double es = this.epsilon.getValue(id);
				this.epsilon.setValue(id, 1-es);
			}
			ids = this.gm.getFacts();
			while(ids.hasNext()) {
				Integer id = ids.next();
				double af = this.alpha.getValue(id);
				this.alpha.setValue(id, 1-af);
			}
		}
	}
}
