/**
 * ApproxThreeStepsMethod.java in dataCorrob
 * Copyright (C) by INRIA-Saclay, Alban Galland, 18 nov. 2008, All rights reserved
 */
package fr.inria.gemo.dataCorrob.method.probabilityMethod;

import java.util.Iterator;

import fr.inria.gemo.dataCorrob.arguments.Arguments;
import fr.inria.gemo.dataCorrob.data.Link;
import fr.inria.gemo.dataCorrob.dataManager.GraphManager;

/**
 * This class implements 
 */
public class ApproxThreeStepsMethod extends ThreeStepsMethod {

	/**
	 * This function is the standard constructor of the class.
	 * @param gm
	 * @param args
	 */
	public ApproxThreeStepsMethod(GraphManager gm, Arguments args) {
		super(gm, args);
	}

	@Override
	protected void computeAlphaFact(int f) {
		double af=0.5;
		double sumAlpha=0;
		double sumF=this.gm.getNbPosChildren(f)+this.gm.getNbNegChildren(f);
		double ef = this.epsilon.getValue(f);
		Iterator <Link> ls = this.gm.getLinks(f);
		while(ls.hasNext()) {
			Link l = ls.next();
			double es = this.epsilon.getValue(l.getTo());
			if (l.isPos()) {
				sumAlpha += 1-es*ef;
			} else if (l.isNeg()) {
				sumAlpha += es*ef;
			}
		}
		if(sumF!=0) {
			af = sumAlpha/sumF;
		}
		this.alpha.setValue(f, af);
	}

	@Override
	protected void computeEpsilonFact(int f) {
		double af = this.alpha.getValue(f);
		double ef=0.5;
		double sumef = 0;
		double sumS = 0;
		Iterator <Link> ls = this.gm.getLinks(f);
		while(ls.hasNext()) {
			Link l = ls.next();
			double es = this.epsilon.getValue(l.getTo());
			if(es!=0) {
				if (l.isPos()) {
					sumef += (1-af)/es;
				} else if (l.isNeg()) {
					sumef += af/es;
				}
				sumS++;
			}
		}
		if(sumS>0){
			ef = sumef/sumS;
		}
		this.epsilon.setValue(f,ef);
	}

	@Override
	protected void computeEpsilonSource(int s) {
		double es=0.5;
		double sumes = 0;
		double sumF =0;
		Iterator <Link> ls = this.gm.getLinks(s);
		while(ls.hasNext()) {
			Link l = ls.next();
			double ef = this.epsilon.getValue(l.getTo());
			if(ef!=0) {
				double af = this.alpha.getValue(l.getTo());
				if (l.isPos()) {
					sumes += (1-af)/ef;
				} else if (l.isNeg()) {
					sumes += af/ef;
				}
				sumF++;
			}
		}
		if(sumF>0){
			es=sumes/sumF;
		}
		this.epsilon.setValue(s, es);
	}
}
