/**
 * SyntheticFact.java in dataCorrob
 * Copyright (C) by INRIA-Saclay, Alban Galland, 28 oct. 2008, All rights reserved
 */
package fr.inria.gemo.dataCorrob.data;

import java.util.Vector;

/**
 * This class implements a synthetic fact
 */
public class SyntheticFact extends Fact {
	/**
	 * This field is the internal way to store the parameter for making an error for this node
	 */
	private double epsilon;
	/**
	 * This field is the internal way to store the parameter for forgetting a
	 * positive link about this node
	 */
	private double phiNeg;
	/**
	 * This field is the internal way to store the parameter for forgetting a
	 * negative link about this node
	 */
	private double phiPos;
	

	
	/**
	 * This function is the standard constructor of the class.
	 * @param id
	 * @param isTrue
	 * @param epsilon
	 * @param phiPos
	 * @param phiNeg
	 */
	public SyntheticFact(int id, boolean isTrue, double epsilon, double phiPos,
			double phiNeg,String queryId) {
		super(id, isTrue, epsilon,phiPos,phiNeg,queryId);
		this.phiPos = phiPos;
		this.phiNeg = phiNeg;
		this.epsilon = epsilon;
	}

	/**
	 * This function gives access to the epsilon parameter
	 * @return the epsilon parameter
	 */
	public double getEpsilon() {
		return this.epsilon;
	}
	
	/**
	 * This function gives access to the phi pos parameter
	 * @return the phi pos parameter
	 */
	public double getPhiPos() {
		return this.phiPos;
	}

	/**
	 * This function gives access to the phi neg parameter
	 * @return the phi neg parameter
	 */
	public double getPhiNeg() {
		return this.phiNeg;
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String s = "SyntheticFact " + this.getId() + " : "+this.isTrue()+" (PError="
				+ this.getPError() + ", Epsilon="
				+ this.getEpsilon() + ", PhiPos="
				+ this.getPhiPos() + ", PhiNeg="
				+ this.getPhiNeg() + ", NbPosChildren="
				+ this.getNbPosChildren() + ", NbNegChildren="
				+ this.getNbNegChildren() + ") : ";
		Vector<Link> l = this.getLinks();
		for (int i = 0; i < l.size(); i++) {
			s += l.get(i).getTo() + "(" + l.get(i).getWeight() + ") ";
		}
		return s;
	}
	
}
