/**
 * SumGenericScoreMethod.java in dataCorrob (C) 26 sept. 08 by agalland 
 */
package fr.inria.gemo.dataCorrob.method.genericMethod;

import java.util.Iterator;

import fr.inria.gemo.dataCorrob.arguments.Arguments;
import fr.inria.gemo.dataCorrob.dataManager.GraphManager;

/**
 * This class implements a method where the update of the score of the source is done with a sum
 * @author agalland (Alban Galland) for INRIA Saclay
 */
public abstract class SumGenericMethod extends GenericMethod {

	private static double epsilon=0.9;
	
	/**
	 * This function is the standard constructor of the class.
	 * @param dm the data manager
	 */
	public SumGenericMethod(GraphManager dm,Arguments args) {
		super(dm,args);
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.method.Method#iterate()
	 */
	@Override
	protected void iterate() {
		Iterator <Integer> ids = this.gm.getSources();
		while(ids.hasNext()) {
			Integer id = ids.next();
		    double score = this.score.getValue(id);
		    score = SumGenericMethod.epsilon*score + (1-SumGenericMethod.epsilon)*this.scoreSource(id);
			this.score.setValue(id, score);
		}
		ids = this.gm.getFacts();
		while(ids.hasNext()) {
			Integer id = ids.next();
			this.score.setValue(id, this.scoreFact(id));
		}
	}
	
}
