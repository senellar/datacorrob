/**
 * ConvergenceTest.java in dataCorrob (C) august 08 by agalland
 */
package fr.inria.gemo.dataCorrob.test.printTest;

import java.util.Iterator;

import fr.inria.gemo.dataCorrob.arguments.Arguments;
import fr.inria.gemo.dataCorrob.dataManager.GraphManager;
import fr.inria.gemo.dataCorrob.graphGenerator.GraphGenerator;
import fr.inria.gemo.dataCorrob.method.AllMethods;
import fr.inria.gemo.dataCorrob.method.Method;
import fr.inria.gemo.dataCorrob.test.Test;

/**
 * This class implements a test which make several step on the iteration and
 * print the scores at each steps
 * @author agalland (Alban Galland) for Inria Saclay
 */
public class ConvergenceTest extends Test {
	/**
	 * This function is the standard constructor of the class.
	 * @param args the arguments of the program
	 */
	public ConvergenceTest(Arguments args) {
		super(args);
	}

	/**
	 * This function prints a string which is the result of the test
	 * @param dm the data manager
	 * @param m the method used in the test
	 * @param the number of the iteration
	 * @return the corresponding string
	 */
	private void print(GraphManager gm,Method m,int i) {
		if(m.getClass().equals(AllMethods.class)) {
			this.out.println(PrintTest.toString(this.getClass())+ " ("+ (i * (this.args.nbTotIter / this.args.nbSteps)) +" iterations) : run all the methods");
			Iterator <Method> ms = ((AllMethods) m).getMethods();
			while(ms.hasNext()) {
				this.out.println(this.printMethod(gm,ms.next()));
			}
		} else {
			this.out.print(PrintTest.toString(this.getClass())+" ("+ (i * (this.args.nbTotIter / this.args.nbSteps)) +" iterations) with method "+Method.toString(m.getClass())+" : ");
			this.out.println(this.printMethod(gm,m));
		}
	}

	/**
	 * This function computes a string which is the result of the test
	 * @param dm the data manager
	 * @param m the method used in the test
	 * @return the corresponding string
	 */
	private String printMethod(GraphManager gm,Method m) {
		String s="\t ";
		Iterator<Integer> nodes = gm.getNodes();
		while (nodes.hasNext()) {
			int id = nodes.next();
			s +=m.getScore(id) + "\t";
		}
		return s;
	}

	@Override
	protected void runTest(GraphManager gm ) {
		Method m = Method.create(this.args.typeMethod, this.args, gm);
		m.init();
		this.print(gm,m,0);
		for (int i = 1; i < this.args.nbSteps + 1; i++) {
			m.iterate(this.args.nbTotIter / this.args.nbSteps);
			this.print(gm,m,i);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.test.Test#runMultiTest()
	 */
	@Override
	protected void runMultiTest() {
		GraphManager gm = GraphGenerator.create(this.args).constructGraph();
		this.runTest(gm);
		this.out.println("Warning(ConvergenceTest) : no aggregation available, only one test run");
	}
}
