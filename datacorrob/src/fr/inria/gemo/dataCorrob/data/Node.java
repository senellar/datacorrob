/**
 * Node.java in dataCorrob C) 25 juil. 08 by agalland
 */
package fr.inria.gemo.dataCorrob.data;

import java.util.Vector;

/**
 * This class implements a node of the graph.
 * @author agalland (Alban Galland) for Inria Saclay
 */
public abstract class Node {
	/**
	 * This field is the identifiant of the node
	 */
	protected int id;
	/**
	 * This field is the internal way to store the probability to make an error
	 * on the fact
	 */
	protected double pError;
	
	protected double pForgPos;
	
	protected double pForgNeg;
	
	
	/**
	 * This field is the internal way to store the links coming from this node
	 */
	private Vector<Link> links;
	/**
	 * This field is the internal way to store the number of negatives children
	 * of the node
	 */
	private int nbNegChildren;
	/**
	 * This field is the internal way to store the number of positive children
	 * of the node
	 */
	private int nbPosChildren;

	/**
	 * This function is the standard constructor of the class.
	 * @param id the identifiant of the node
	 * @param pError the probability to make an error
	 * @param pForgPos the probability of forget a positive link about this
	 *            node
	 * @param pForgNeg the probability of forget a negative link about this
	 *            node
	 */
	protected Node(int id, double pError, double pForgPos, double pForgNeg) {
		this.id = id;
		this.pError = pError;
		this.pForgPos = pForgPos;
		this.pForgNeg = pForgNeg;
		this.links = new Vector<Link>();
		this.nbPosChildren = 0;
		this.nbNegChildren = 0;
	}

	/**
	 * This function answers if the node is true (usually used for a fact)
	 * @return the truth of the node
	 */
	public boolean isTrue() {
		return false;
	}

	/**
	 * This function answers if the node is a fact
	 * @return true if the node is a fact, false else
	 */
	public boolean isFact() {
		return false;
	}

	/**
	 * This function answers if the node is a source
	 * @return true if the node is a source, false else
	 */
	public boolean isSource() {
		return false;
	}

	/**
	 * This function gives access to the number of positive children of the node
	 * @return the number of children of the node
	 */
	public int getNbPosChildren() {
		return this.nbPosChildren;
	}

	/**
	 * This function gives access to the number of negative children of the node
	 * @return the number of children of the node
	 */
	public int getNbNegChildren() {
		return this.nbNegChildren;
	}

	/**
	 * This function gives access to the id of the node
	 * @return the id of the node
	 */
	public int getId() {
		return this.id;
	}
	
	/**
	 * This function gives access to the probability to make an error
	 * @return the probability to make an error
	 */
	public double getPError() {
		return this.pError;
	}
	
	public double getPForgPos() {
		return this.pForgPos;
	}
	
	public double getPForgNeg() {
		return this.pForgNeg;
	}
	
	/**
	 * This function gives access to the probability to make an error
	 * @param pError the new value of the probability
	 */
	public void setPError(double pError) {
		this.pError = pError;
	}
	
	public void setPForgPos(double pForgPos) {
		this.pForgPos = pForgPos;
	}
	
	public void setPForgNeg(double pForgNeg) {
		this.pForgNeg = pForgNeg;
	}

	/**
	 * This function adds a link to the list of links of the node
	 * @param l the link to add to the list
	 */
	public void addLink(Link l) {
		this.links.add(l);
		if (l.isPos()) {
			this.nbPosChildren++;
		} else if (l.isNeg()) {
			this.nbNegChildren++;
		} else {
			System.err
			.println("Warning : add a link with null weight to a node");
		}
	}

	/**
	 * This function gives access to the list of links of the node
	 * @return the list of links of the node
	 */
	public Vector<Link> getLinks() {
		return this.links;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object o2) {
		if (Node.class.isAssignableFrom(o2.getClass())) {
			return false;
		} else {
			Node n = (Node) o2;
			return (this.id == n.id);
		}
	}
}
