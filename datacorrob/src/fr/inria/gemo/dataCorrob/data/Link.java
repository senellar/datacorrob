/**
 * link.java in dataCorrob (C) august 08 by agalland
 */
package fr.inria.gemo.dataCorrob.data;

/**
 * this class implements a weighted link between a source and a fact
 * @author agalland (Alban Galland) for Inria Saclay
 */
public class Link {
	/**
	 * This field is the id of the node where the link come from
	 */
	private int from;
	/**
	 * This field is the id of the node where the link go to
	 */
	private int to;
	/**
	 * this field is the internal way to store the weight of the link
	 */
	private double weight;

	/**
	 * This function is the standard constructor of the class.
	 * @param from the id of the node where the link come from
	 * @param to the id of the node where the link go to
	 * @param weight the weight of the link
	 */
	public Link(int from, int to, double weight) {
		this.from = from;
		this.to = to;
		this.weight = weight;
	}

	/**
	 * This function gives access to the id of the node where the link come from
	 * @return the if of the node where the link come from
	 */
	public int getFrom() {
		return this.from;
	}

	/**
	 * This function gives access to the id of the node where the link go to
	 * @return the id of the node where the link go to
	 */
	public int getTo() {
		return this.to;
	}

	/**
	 * This function gives access to the weight of the link
	 * @return the weight of the link
	 */
	public double getWeight() {
		return this.weight;
	}
	
	/**
	 * This function answers if the link is a positive link
	 * @return true if the link is positive
	 */
	public boolean isPos() {
		return this.weight>0;
	}

	/**
	 * This function answers if the link is a negative link
	 * @return true if the link is a negative link
	 */
	public boolean isNeg() {
		return this.weight<0;
	}
}
