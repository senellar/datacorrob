/**
 * GenericMethod.java in dataCorrob (C) 26 sept. 08 by agalland 
 */
package fr.inria.gemo.dataCorrob.method.genericMethod;

import java.util.Iterator;

import fr.inria.gemo.dataCorrob.arguments.Arguments;
import fr.inria.gemo.dataCorrob.dataManager.AnnotationManager;
import fr.inria.gemo.dataCorrob.dataManager.GraphManager;
import fr.inria.gemo.dataCorrob.method.Method;

/**
 * This class implements 
 * @author agalland (Alban Galland) for INRIA Saclay
 */
public abstract class GenericMethod extends Method {

	protected AnnotationManager score;
	
	/**
	 * This function is the standard constructor of the class.
	 * @param dm the data manager
	 */
	protected GenericMethod(GraphManager gm,Arguments args) {
		super(gm,args);
		this.score = AnnotationManager.create(args);
	}
	
/**
 * This function computes an initialization value for the sources
 * @param id the id of the source
 * @return the initialization value
 */
	protected abstract double initSource(int id);
	
	/**
	 * This function computes an initialization value for the facts
	 * @param id the id of the fact
	 * @return the initialization value
	 */
	protected abstract double initFact(int id);
	
	/**
	 * This function computes a score for the sources
	 * @param id the id of the source
	 * @return the score value
	 */
	protected abstract double scoreSource(int id);
	
	/**
	 * This function computes a score for the facts
	 * @param id the id of the fact
	 * @return the score value
	 */
	protected abstract double scoreFact(int id);

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.method.Method#getScore(int)
	 */
	@Override
	public double getSimpleScore(int id) {
		return this.score.getValue(id);
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.method.Method#init()
	 */
	@Override
	public void init() {
		Iterator <Integer> ids = this.gm.getSources();
		while(ids.hasNext()) {
			Integer id = ids.next();
			this.score.setValue(id, this.initSource(id));
		}
		ids = this.gm.getFacts();
		while(ids.hasNext()) {
			Integer id = ids.next();
			this.score.setValue(id, this.initFact(id));
		}
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.method.Method#iterate()
	 */
	@Override
	protected void iterate() {
		Iterator <Integer> ids = this.gm.getSources();
		while(ids.hasNext()) {
			Integer id = ids.next();
			this.score.setValue(id, this.scoreSource(id));
		}
		ids = this.gm.getFacts();
		while(ids.hasNext()) {
			Integer id = ids.next();
			this.score.setValue(id, this.scoreFact(id));
		}
	}
}
