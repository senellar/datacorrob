/**
 * QCM1GraphGenerator.java in dataCorrob
 * Copyright (C) by INRIA-Saclay, Alban Galland, 12 nov. 2008, All rights reserved
 */
package fr.inria.gemo.dataCorrob.graphGenerator.realDataGraphGenerator;

import java.util.Iterator;
import java.util.Vector;

import fr.inria.gemo.dataCorrob.arguments.Arguments;

/**
 * This class implements 
 */
public class QCM1GraphGenerator extends RealDataGraphGenerator {

	public QCM1GraphGenerator(Arguments args) {
		super(args);
	}

	@Override
	public String getFile() {
		return "data/qcm1.xml";
	}

	@Override
	public Iterator<String> getSourceNames() {
		Vector <String> names = new Vector <String> ();
		for(int i=0;i<601;i++) {
			names.add("e"+i);
		}
		return names.iterator();
	}
}
