/**
 * RealFact.java in dataCorrob
 * Copyright (C) by INRIA-Saclay, Alban Galland, 28 oct. 2008, All rights reserved
 */
package fr.inria.gemo.dataCorrob.data;

import java.util.Vector;

/**
 * This class implements 
 */
public class RealFact extends Fact {
	
	private String query;
	
	private String answer;
	
	private double weight;
	
	public RealFact(int id, String queryId, String query, String answer, boolean isTrue, double weight) {
		super(id, isTrue,1,1,1,queryId);
		this.query = query;
		this.answer = answer;
		this.weight = weight;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public String getQuery() {
		return this.query;
	}
	
	public String getAnswer() {
		return this.answer;
	}
	
	public double getWeight() {
		return this.weight;
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String s = "RealFact " + this.getId() + " : "+this.isTrue()+" "+this.getQueryId()+" "+this.getQuery()+"="+this.getAnswer()+" (PError="
				+ this.getPError() + ", NbPosChildren="
				+ this.getNbPosChildren() + ", NbNegChildren="
				+ this.getNbNegChildren() + ") : ";
		Vector<Link> l = this.getLinks();
		for (int i = 0; i < l.size(); i++) {
			s += l.get(i).getTo() + "(" + l.get(i).getWeight() + ") ";
		}
		return s;
	}
}
