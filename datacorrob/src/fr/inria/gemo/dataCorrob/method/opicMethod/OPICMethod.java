/**
 * OPICPropagator.java in dataCorrob (C) 29 juil. 08 by agalland
 */
package fr.inria.gemo.dataCorrob.method.opicMethod;

import java.util.Iterator;

import fr.inria.gemo.dataCorrob.arguments.Arguments;
import fr.inria.gemo.dataCorrob.data.Link;
import fr.inria.gemo.dataCorrob.dataManager.AnnotationManager;
import fr.inria.gemo.dataCorrob.dataManager.GraphManager;
import fr.inria.gemo.dataCorrob.method.Method;

/**
 * This class is the standard OPIC propagator, ignoring positive
 * @author agalland (Alban Galland) for Inria Saclay
 */
public class OPICMethod extends Method {
	/**
	 * This field is the maximum money of the virtual node before to empty it
	 */
	private static double maxMoneyVirtual = 10;
	/**
	 * This field is the rate of money which must be send to the virtual node
	 */
	private static double epsilon = 0.1;
	/**
	 * This field is the maximum money owned by a fact
	 */
	private double maxPosBankOfFacts;
	/**
	 * This field is the maximum money owned by a source
	 */
	private double maxPosBankOfSources;
	/**
	 * This field is the average number of sources positively linked to a fact
	 */
	private double avgNbPosSources;
	/**
	 * This field is the positive cash of the virtual node
	 */
	private double posCashVirtual;
	/**
	 * This field is the internal way to store the positive cash of the nodes
	 */
	private AnnotationManager posCash;
	/**
	 * This field is the internal way to store the positive bank of the nodes
	 */
	private AnnotationManager posBank;

	/**
	 * This function is the standard constructor of the class
	 * @param dm the data manager for the propagation
	 */
	public OPICMethod(GraphManager gm,Arguments args) {
		super(gm,args);
		this.posCash =  AnnotationManager.create(args);
		this.posBank = AnnotationManager.create(args);
		this.maxPosBankOfFacts=0;
		this.maxPosBankOfSources=0;
		this.avgNbPosSources=0;
		double sumSources=0;
		double nbFacts=0;
		Iterator <Integer> ids = this.gm.getFacts();
		while(ids.hasNext()) {
			nbFacts++;
			Integer id = ids.next();
			Iterator <Link> ls = this.gm.getLinks(id);
			while(ls.hasNext()) {
				Link l =ls.next();
				if(l.isPos()) {
					sumSources++;
				}
			}
		}
		this.avgNbPosSources=sumSources/nbFacts;
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.method.Method#getScore(int)
	 */
	@Override
	public double getSimpleScore(int id) {
		if (this.gm.getIsFact(id)) {
			return 2 * (this.posBank.getValue(id)/this.maxPosBankOfFacts) - 1;
		} else {
			return this.posBank.getValue(id)/this.maxPosBankOfSources;
		}
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.method.Method#init()
	 */
	@Override
	public void init() {
		this.posCash.reset(0);
		this.posBank.reset(0);
		Iterator<Integer> nodes = this.gm.getNodes();
		while (nodes.hasNext()) {
			Integer id = nodes.next();
			double posCash = this.gm.getNbPosChildren(id)
			/ this.avgNbPosSources;
			this.posCash.setValue(id,posCash);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.method.Method#iterate()
	 */
	@Override
	protected void iterate() {
		Iterator <Integer> ids = this.gm.getNodes();
		while(ids.hasNext()) {
			Integer id = ids.next();
			this.manageVirtualCash();
			double posCash = this.posCash.getValue(id);
			double posBank = this.posBank.addToValue(id, posCash);
			if(this.gm.getIsFact(id)) {
			if(posBank>this.maxPosBankOfFacts) {
				this.maxPosBankOfFacts = posBank;
			}
			} else {
				if(posBank>this.maxPosBankOfSources) {
					this.maxPosBankOfSources = posBank;
				}
			}
			int nbchildren = this.gm.getNbPosChildren(id);
			if (nbchildren > 0) {
				Iterator<Link> links = this.gm.getLinks(id);
				while (links.hasNext()) {
					Link l = links.next();
					if (l.isPos()) {
						this.posCash.addToValue(l.getTo(),(1 - OPICMethod.epsilon) * posCash
								/ nbchildren);
					}
				}
				this.posCashVirtual += OPICMethod.epsilon * posCash;
			} else {
				this.posCashVirtual += posCash;
			}

		}
	}

	/**
	 * This private function checks the virtual cash and distributes it to all the nodes
	 */
	private void manageVirtualCash() {
		if (this.posCashVirtual > OPICMethod.maxMoneyVirtual) {
			double money = this.posCashVirtual / this.gm.getNbNodes();
			Iterator<Integer> nodes = this.gm.getNodes();
			while (nodes.hasNext()) {
				int id = nodes.next();
				this.posCash.addToValue(id, money);
			}
			this.posCashVirtual = 0;
		}
	}
}
