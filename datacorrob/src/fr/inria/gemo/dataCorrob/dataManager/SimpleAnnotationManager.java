/**
 * SimpleAnnotationManager.java in dataCorrob (C) 25 sept. 08 by agalland 
 */
package fr.inria.gemo.dataCorrob.dataManager;

import java.util.Vector;

import fr.inria.gemo.dataCorrob.arguments.Arguments;

/**
 * This abstract class represents a generic data manager which gives access to the graph and its annotation
 * @author agalland (Alban Galland) for INRIA Saclay
 */
public class SimpleAnnotationManager extends AnnotationManager{
	/**
	 * This field is the internal way to store the annotations
	 */
	private Vector <Double> annotations;

	protected SimpleAnnotationManager(Arguments args) {
		this.annotations = new Vector <Double>();
		this.annotations.setSize(args.nbFacts+args.nbSources);
	}

	@Override
	public double addToValue(int id, double value) {
		Double d = this.annotations.get(id);
		if(d==null){
			d = 0.0;
		}
		d += value;
		this.annotations.set(id, d);
		return d;
	}

	@Override
	public double getValue(int id) {
		Double d = this.annotations.get(id);
		if (d== null) {
			return 0;
		} else {
			return d;
		}
	}

	@Override
	public void reset(double value) {
		for(int i=0;i<this.annotations.size();i++) {
			this.annotations.set(i, value);
		}
	}

	@Override
	public double setValue(int id, double value) {
		Double d = this.annotations.set(id, value);
		if (d==null) {
			return 0;
		} else {
			return d;
		}
	}


}
