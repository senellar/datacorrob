/**
 * VirtualPosNegPropagator.java in dataCorrob (C) august 08 by agalland
 */
package fr.inria.gemo.dataCorrob.method.baseLineMethod;

import fr.inria.gemo.dataCorrob.arguments.Arguments;
import fr.inria.gemo.dataCorrob.dataManager.GraphManager;

/**
 * This class implements a virtual propagator, which use only the number of
 * children
 * @author agalland (Alban Galland) for Inria Saclay
 */
public class BaseLinePosNegMethod extends BaseLineMethod {
	/**
	 * This function is the standard constructor of the class.
	 * @param dm the data manager
	 */
	public BaseLinePosNegMethod(GraphManager gm,Arguments args) {
		super(gm,args);
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.method.Method#getScore(int)
	 */
	@Override
	public double getSimpleScore(int id) {
		if(this.gm.getIsFact(id)) {
			double score = this.gm.getNbPosChildren(id) / (new Double(this.gm
					.getNbChildren(id)));
			return 2*score-1;
		} else {
			return 1;
		}
	}
}
