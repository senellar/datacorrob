/**
 * VirtualPosPropagator.java in dataCorrob (C) august 08 by agalland
 */
package fr.inria.gemo.dataCorrob.method.baseLineMethod;

import java.util.Iterator;

import fr.inria.gemo.dataCorrob.arguments.Arguments;
import fr.inria.gemo.dataCorrob.dataManager.GraphManager;

/**
 * This class implements a virtual propagator, which compute the score using
 * only the number of children
 * @author agalland (Alban Galland) for Inria Saclay
 */
public class BaseLinePosMethod extends BaseLineMethod {
	/**
	 * Maximum number of sources positively linked to a fact
	 */
	private double maxNbPosSources;

	/**
	 * This function is the standard constructor of the class.
	 * @param gm the data manager
	 */
	public BaseLinePosMethod(GraphManager gm,Arguments args) {
		super(gm,args);
		this.maxNbPosSources=0;
		Iterator <Integer> ids = gm.getFacts();
		while(ids.hasNext()) {
			Integer id = ids.next();
			double nbPosSources=this.gm.getNbPosChildren(id);
			if (nbPosSources>this.maxNbPosSources) {
				this.maxNbPosSources = nbPosSources;
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.propagator.Propagator#computeScore(int)
	 */
	@Override
	public double getSimpleScore(int id) {
		if(this.gm.getIsFact(id)) {
			double score = this.gm.getNbPosChildren(id) / this.maxNbPosSources;
			return 2*score-1;
		} else {
			return 1;
		}
	}
}
