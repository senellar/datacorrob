/**
 * ProductPropagator.java in dataCorrob (C) 24 sept. 08 by agalland 
 */
package fr.inria.gemo.dataCorrob.method.genericMethod;

import java.util.Iterator;

import fr.inria.gemo.dataCorrob.arguments.Arguments;
import fr.inria.gemo.dataCorrob.data.Link;
import fr.inria.gemo.dataCorrob.dataManager.GraphManager;

/**
 * This class implements the product method
 * @author agalland (Alban Galland) for INRIA Saclay
 */
public class ProductMethod extends GenericMethod {

	/**
	 * This field is a factor used for initialization
	 */
	private static double epsilon = 0.1;

	/**
	 * This function is the standard constructor of the class.
	 * @param dm the data manager
	 */
	public ProductMethod(GraphManager gm,Arguments args) {
		super(gm,args);
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.method.genericMethod.GenericMethod#initFact(int)
	 */
	@Override
	protected double initFact(int id) {
		//return this.gm.getNbPosChildren(id)/((double) (this.gm.getNbPosChildren(id)+this.gm.getNbNegChildren(id)));
		return 0.5+ProductMethod.epsilon;
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.method.genericMethod.GenericMethod#initSource(int)
	 */
	@Override
	protected double initSource(int id) {
		//return this.gm.getNbPosChildren(id)/((double) (this.gm.getNbPosChildren(id)+this.gm.getNbNegChildren(id)));
		return 0.5;
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.method.genericMethod.GenericMethod#scoreFact(int)
	 */
	@Override
	protected double scoreFact(int id) {
		double prods = 1;
		double antiprods = 1;
		Iterator<Link> links = this.gm.getLinks(id);
		while (links.hasNext()) {
			Link l = links.next();
			double posCash = this.score.getValue(l.getTo());
			if(l.isPos()) {
				prods *= posCash;
				antiprods *= (1-posCash);
			} else {
				prods *=(1-posCash);
				antiprods *= posCash;
			}
		}
		double estf = this.score.getValue(id);
		return (estf*prods)/(estf*prods+(1-estf)*antiprods);
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.method.genericMethod.GenericMethod#scoreSource(int)
	 */
	@Override
	protected double scoreSource(int id) {
		double sumf = 0;
		int nl=0;
		Iterator<Link> links = this.gm.getLinks(id);
		while (links.hasNext()) {
			nl++;
			Link l = links.next();
			double posCash = this.score.getValue(l.getTo());
			if(l.isPos()) {
				sumf += posCash;
			} else {
				sumf += (1-posCash);
			}
		}
		return sumf/nl;
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.method.Method#getScore(int)
	 */
	@Override
	public double getSimpleScore(int id) {
		if (this.gm.getIsFact(id)) {
			return 2*this.score.getValue(id)-1;
		} else {
			return this.score.getValue(id);
		}
	}
}
