/**
 * YahooEngineExtractor.java in dataCorrob
 * Copyright (C) by INRIA-Saclay, Alban Galland, 28 oct. 2008, All rights reserved
 */
package fr.inria.gemo.dataCorrob.graphGenerator.realDataGraphGenerator.searchEnginesGraphGenerator.searchEngineExtractor;


/**
 * This class implements 
 */
public class YahooEngineExtractor extends HtmlEngineExtractor {

	/* (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.graphGenerator.realDataGraphGenerator.searchEnginesGraphGenerator.HtmlEngineManager#getUrl(java.lang.String)
	 */
	@Override
	public String getUrl(String q) {
		return "http://search.yahoo.com/search?p="+q.replace(" ", "+");
	}

	@Override
	public String filtering(String s) {
		String fs = s.replaceAll("&copy; 2008 Yahoo!", "Yahoo!");
		fs = fs.replaceAll("PDT 2008 -->", "-->");
		return fs;
	}
}
