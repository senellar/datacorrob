/**
 * BestAnswersSearchEnginesGraphGenerator.java in dataCorrob
 * Copyright (C) by INRIA-Saclay, Alban Galland, 4 nov. 2008, All rights reserved
 */
package fr.inria.gemo.dataCorrob.graphGenerator.realDataGraphGenerator.searchEnginesGraphGenerator;

import java.util.Enumeration;
import java.util.Hashtable;

import fr.inria.gemo.dataCorrob.arguments.Arguments;
import fr.inria.gemo.dataCorrob.data.RealFact;
import fr.inria.gemo.dataCorrob.data.RealSource;
import fr.inria.gemo.dataCorrob.dataManager.GraphManager;

/**
 * This class implements 
 */
public class BestAnswersSearchEnginesGraphGenerator extends
SearchEnginesGraphGenerator {

	public BestAnswersSearchEnginesGraphGenerator(Arguments args) {
		super(args);
	}

	@Override
	protected int linkSource(GraphManager gm, Enumeration <RealFact> facts, Hashtable <String,RealFact> knownfacts, RealSource s,int id) {
		Hashtable <String,RealFact> bestAnswers = new Hashtable <String,RealFact>();
		while(facts.hasMoreElements()) {
			RealFact f = facts.nextElement();
			String key =  f.getQueryId();
			if (bestAnswers.containsKey(key)) {
				RealFact ff = bestAnswers.get(key);
				if (f.getWeight()>ff.getWeight()) {
					bestAnswers.put(key, f);
				}
			} else {
				if (f.getWeight()>1.0) {
					bestAnswers.put(key, f);
				}
			}
		}
		Enumeration <RealFact> bestfacts = bestAnswers.elements();
		while(bestfacts.hasMoreElements()) {
			RealFact f = bestfacts.nextElement();
			String key = f.getAnswer()+f.getQueryId();
			if (knownfacts.containsKey(key)) {
				f = knownfacts.get(key);
			} else {
				f.setId(id);
				id++;
				gm.addFact(f);
				knownfacts.put(key, f);
			}
			gm.addChildToNode(f.getId(), s.getId(), 1);
			gm.addChildToNode(s.getId(), f.getId(), 1);
		}
		return id;
	}
}
