/**
 * SogouEngineExtractor.java in dataCorrob
 * Copyright (C) by INRIA-Saclay, Alban Galland, 29 oct. 2008, All rights reserved
 */
package fr.inria.gemo.dataCorrob.graphGenerator.realDataGraphGenerator.searchEnginesGraphGenerator.searchEngineExtractor;

/**
 * This class implements 
 */
public class SogouEngineExtractor extends HtmlEngineExtractor {

	/* (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.graphGenerator.realDataGraphGenerator.searchEnginesGraphGenerator.searchEngineExtractor.HtmlEngineExtractor#getUrl(java.lang.String)
	 */
	@Override
	public String getUrl(String q) {
		return "http://www.sogou.com/web?query="+q.replace(" ", "+");
	}

	@Override
	public String filtering(String s) {
		String fs = s.replaceAll("&copy; 2008 SOGOU.COM","");
		fs = s.replaceAll("expires=[\\w\\s,]*2046","");
		return fs;
	}

}
