/**
 * AlexaEngineExtractor.java in dataCorrob
 * Copyright (C) by INRIA-Saclay, Alban Galland, 29 oct. 2008, All rights reserved
 */
package fr.inria.gemo.dataCorrob.graphGenerator.realDataGraphGenerator.searchEnginesGraphGenerator.searchEngineExtractor;


/**
 * This class implements 
 */
public class AlexaEngineExtractor extends HtmlEngineExtractor {

	/* (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.graphGenerator.realDataGraphGenerator.searchEnginesGraphGenerator.HtmlEngineManager#getUrl(java.lang.String)
	 */
	@Override
	public String getUrl(String q) {
		return "http://www.alexa.com/search?q="+q.replace(" ", "+");
	}

	@Override
	public String filtering(String s) {
		return s;
	}

}
