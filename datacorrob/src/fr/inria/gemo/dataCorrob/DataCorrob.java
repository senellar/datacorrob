/**
 * DataCorrob.java in dataCorrob (C) 25 juil. 08 by agalland
 */
package fr.inria.gemo.dataCorrob;

import fr.inria.gemo.dataCorrob.arguments.Arguments;
import fr.inria.gemo.dataCorrob.test.Test;

/**
 * This class is the main class for the dataCorrob programs. You may run the
 * main function of this class to use the program.
 * @author agalland (Alban Galland) for INRIA Saclay
 */
public class DataCorrob {

	/**
	 * This class is the main function of the program
	 * @param args no arguments available yet
	 */
	public static void main(String[] args) {
		String file;
		if ((args == null) || (args.length == 0)) {
			file = "arguments.xml";
		} else {
			file = args[0];
		}
		Arguments arguments = new Arguments(file);
		Test t = Test.create(arguments.typeTest,arguments);
	    t.run();
	}
}
