/**
 * ErrorTest.java in dataCorrob (C) august 08 by agalland
 */
package fr.inria.gemo.dataCorrob.test.printTest;

import java.util.Iterator;
import java.util.Hashtable;

import fr.inria.gemo.dataCorrob.arguments.Arguments;
import fr.inria.gemo.dataCorrob.dataManager.GraphManager;
import fr.inria.gemo.dataCorrob.method.AllMethods;
import fr.inria.gemo.dataCorrob.method.Method;

/**
 * This class implements a test which compute the error done by the propagator
 * estimation
 * @author agalland (Alban Galland) for Inria Saclay
 */
public class ErrorTest extends PrintTest {
	/**
	 * This field is the internal way to store the results of the test for multi runs
	 */
	private Hashtable <String,Double> scores;

	/**
	 * This function is the standard constructor of the class.
	 * @param args the arguments of the program
	 */
	public ErrorTest(Arguments args) {
		super(args);
		this.scores= new Hashtable <String,Double> ();
		for(int i=0;i<AllMethods.methods.length;i++) {
			this.scores.put(Method.toString(AllMethods.methods[i]), 0.0);
		}
	}

	/**
	 * This function is the internal way to compute the result of the test
	 * @param dm the data manager
	 * @param m the method used
	 * @return the error as a result of the test
	 */
	protected double computeError(GraphManager gm,Method m){
		double error = 0;
		Iterator<Integer> nodes = gm.getFacts();
		while (nodes.hasNext()) {
			int id = nodes.next();
			Boolean isTrue = m.getTruth(id);
			if(!isTrue.equals(gm.getIsTrue(id))) {
				error++;
			}
		}
		return error/gm.getNbFacts();
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.test.Test#print(fr.inria.gemo.dataCorrob.dataManager.DataManager, fr.inria.gemo.dataCorrob.method.Method)
	 */
	@Override
	protected String print(GraphManager gm,Method m) {
		double posErrors = 0;
		double negErrors = 0;
		Iterator<Integer> nodes = gm.getFacts();
		while (nodes.hasNext()) {
			int id = nodes.next();
			Boolean isTrue = m.getTruth(id);
			if (isTrue && !gm.getIsTrue(id)) {
				posErrors++;
			}else if(!isTrue && gm.getIsTrue(id)) {
				negErrors++;
			}
		}
		return "Error="+this.computeError(gm,m)+" nbErrors="+(posErrors+negErrors)+" nbPosErrors="+posErrors+"("+posErrors/gm.getNbNegFacts()+") nbNegErrors="+negErrors+"("+negErrors/gm.getNbPosFacts()+")";
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.test.Test#printMulti()
	 */
	@Override
	protected void printMulti() {
		this.out.println(PrintTest.toString(this.getClass())+" with "+this.args.nbRuns+" runs");
		if(args.typeMethod.equals(AllMethods.class)) {
			for(int i=0;i<AllMethods.methods.length;i++) {
				String c = Method.toString(AllMethods.methods[i]);
				System.out.println("Method "+c+" : AvgError="+(this.scores.get(c)/this.args.nbRuns));
			}
		} else {
			String c = Method.toString(this.args.typeMethod);
			this.out.println("Method "+c+" : AvgError="+(this.scores.get(c)/this.args.nbRuns));
		}
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.test.Test#save(fr.inria.gemo.dataCorrob.dataManager.DataManager, fr.inria.gemo.dataCorrob.method.Method)
	 */
	@Override
	protected void save(GraphManager gm,Method m) {
		String c = Method.toString(m.getClass());
		double score = this.scores.get(c)+this.computeError(gm,m);
		this.scores.put(c, score);
	}
}
