/**
 * BaseLineMethod.java in dataCorrob (C) 26 sept. 08 by agalland 
 */
package fr.inria.gemo.dataCorrob.method.baseLineMethod;

import fr.inria.gemo.dataCorrob.arguments.Arguments;
import fr.inria.gemo.dataCorrob.dataManager.GraphManager;
import fr.inria.gemo.dataCorrob.method.Method;

/**
 * This class implements 
 * @author agalland (Alban Galland) for INRIA Saclay
 */
public abstract class BaseLineMethod extends Method {

	protected BaseLineMethod(GraphManager gm,Arguments args) {
		super(gm,args);
	}

	/* (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.method.Method#init()
	 */
	@Override
	public void init() {
	}

	/* (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.method.Method#iterate()
	 */
	@Override
	protected void iterate() {
	}
}
