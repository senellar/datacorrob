/**
 * RealSource.java in dataCorrob
 * Copyright (C) by INRIA-Saclay, Alban Galland, 28 oct. 2008, All rights reserved
 */
package fr.inria.gemo.dataCorrob.data;

import java.util.Vector;

/**
 * This class implements 
 */
public class RealSource extends Source {

	private String name;
	
	/**
	 * This function is the standard constructor of the class.
	 * @param id
	 */
	public RealSource(int id, String name) {
		super(id,1,1,1);
		this.name = name;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getName() {
		return this.name;
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String s = "RealSource " + this.getId() + " : "+ this.getName()+" (PError="
				+ this.getPError() + ", NbPosChildren="
				+ this.getNbPosChildren() + ", NbNegChildren="
				+ this.getNbNegChildren() + ") : ";
		Vector<Link> l = this.getLinks();
		for (int i = 0; i < l.size(); i++) {
			s += l.get(i).getTo() + "(" + l.get(i).getWeight() + ") ";
		}
		return s;
	}
}
