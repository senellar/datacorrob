/**
 * PrecisionRecallTest.java in dataCorrob (C) august 08 by agalland
 */
package fr.inria.gemo.dataCorrob.test.printTest;

import java.util.Arrays;
import java.util.Iterator;

import fr.inria.gemo.dataCorrob.arguments.Arguments;
import fr.inria.gemo.dataCorrob.dataManager.GraphManager;
import fr.inria.gemo.dataCorrob.method.Method;
import fr.inria.gemo.dataCorrob.test.ScoredFact;

/**
 * This class implements a test which summarizes the precision-recall curve with
 * an error score
 * @author agalland (Alban Galland) for Inria Saclay
 */
public class PrecisionRecallTest extends ErrorTest {

	/**
	 * This function is the standard constructor of the class.
	 * @param args the arguments of the program
	 */
	public PrecisionRecallTest(Arguments args) {
		super(args);
	}

	protected double computeError(GraphManager gm, Method m) {
		ScoredFact[] facts = new ScoredFact[gm.getNbFacts()];
		int k = 0;
		int nbTotTrueFacts = 0;
		Iterator<Integer> nodes = gm.getFacts();
		while (nodes.hasNext()) {
			int id = nodes.next();
			facts[k] = new ScoredFact(id, m.getScore(id));
			k++;
			if(gm.getIsTrue(id)) {
				nbTotTrueFacts++;
			}
		}
		Arrays.sort(facts);
		double error = 0;
		double nbTrueFacts = 0;
		int i=0;
		while(nbTrueFacts<nbTotTrueFacts) {
			if (gm.getIsTrue(facts[i].id)) {
				nbTrueFacts++;
			}
			error += 1 - (nbTrueFacts / (i + 1));
			i++;
		}
		return error;
	}
}
