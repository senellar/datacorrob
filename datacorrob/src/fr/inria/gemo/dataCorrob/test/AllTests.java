/**
 * 
 */
package fr.inria.gemo.dataCorrob.test;

import fr.inria.gemo.dataCorrob.arguments.Arguments;
import fr.inria.gemo.dataCorrob.dataManager.GraphManager;
import fr.inria.gemo.dataCorrob.graphGenerator.GraphGenerator;
import fr.inria.gemo.dataCorrob.test.graphTest.PlotEstimatorsSourcesTest;
import fr.inria.gemo.dataCorrob.test.graphTest.PlotEstimatorsTest;
import fr.inria.gemo.dataCorrob.test.graphTest.PlotGraphGenerationTest;
import fr.inria.gemo.dataCorrob.test.graphTest.PlotPrecisionRecallTest;

/**
 * @author alban
 *
 */
public class AllTests extends Test {
	
	/**
	 * This static field is the list of methods executed by all methods
	 */
	public static Class<?> [] tests = {PlotGraphGenerationTest.class,PlotEstimatorsTest.class,PlotEstimatorsSourcesTest.class,PlotPrecisionRecallTest.class};
	/**
	 * @param args
	 */
	public AllTests(Arguments args) {
		super(args);
	}

	/* (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.test.Test#runMultiTest()
	 */
	@Override
	protected void runMultiTest() {
		GraphManager gm = GraphGenerator.create(this.args).constructGraph();
		this.runTest(gm);
		this.out.println("Warning(AllTest) : no aggregation available, only one test run");
	}

	/* (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.test.Test#runTest(fr.inria.gemo.dataCorrob.dataManager.GraphManager)
	 */
	@Override
	protected void runTest(GraphManager gm) {
		for(int i=0;i<tests.length;i++) {
			Test.create(tests[i], args).runTest(gm);
		}
	}

}
