/**
 * Source.java in dataCorrob (C) 25 juil. 08 by agalland
 */
package fr.inria.gemo.dataCorrob.data;

import java.util.Vector;

/**
 * This class implements a source of the graph
 * @author agalland (Alban Galland) for Inria Saclay
 */
public class Source extends Node {
	/**
	 * This function is the standard constructor of the class.
	 * @param id the id of the source
	 */
	public Source(int id, double pError,double pForgPos,double pForgNeg) {
		super(id,pError,pForgPos,pForgNeg);
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.data.Node#isSource()
	 */
	@Override
	public boolean isSource() {
		return true;
	}

	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String s = "Source " + this.getId() + " (NbPosChildren="
				+ this.getNbPosChildren() + ", NbNegChildren="
				+ this.getNbNegChildren() + ") : ";
		Vector<Link> l = this.getLinks();
		for (int i = 0; i < l.size(); i++) {
			s += l.get(i).getTo() + "(" + l.get(i).getWeight() + ") ";
		}
		return s;
	}
}
