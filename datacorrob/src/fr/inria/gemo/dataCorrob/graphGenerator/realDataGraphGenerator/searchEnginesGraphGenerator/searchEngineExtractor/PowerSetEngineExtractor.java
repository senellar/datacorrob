/**
 * PowerSetEngineExtractor.java in dataCorrob
 * Copyright (C) by INRIA-Saclay, Alban Galland, 28 oct. 2008, All rights reserved
 */
package fr.inria.gemo.dataCorrob.graphGenerator.realDataGraphGenerator.searchEnginesGraphGenerator.searchEngineExtractor;

/**
 * This class implements 
 */
public class PowerSetEngineExtractor extends HtmlEngineExtractor {

	/* (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.graphGenerator.realDataGraphGenerator.searchEnginesGraphGenerator.HtmlEngineManager#getUrl(java.lang.String)
	 */
	@Override
	public String getUrl(String q) {
		return "http://www.powerset.com/explore/go/"+q.replace(" ", "-");
	}

	@Override
	public String filtering(String s) {
		String fs = s.replaceAll("Copyright &copy; 2008 Powerset, Inc.","");
		return fs;
	}

}
