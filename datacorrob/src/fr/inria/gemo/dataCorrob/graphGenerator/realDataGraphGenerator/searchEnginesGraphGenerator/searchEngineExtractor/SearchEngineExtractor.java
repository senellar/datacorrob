/**
 * SearchEngineExtractor.java in dataCorrob
 * Copyright (C) by INRIA-Saclay, Alban Galland, 3 nov. 2008, All rights reserved
 */
package fr.inria.gemo.dataCorrob.graphGenerator.realDataGraphGenerator.searchEnginesGraphGenerator.searchEngineExtractor;

import java.io.File;
import java.io.IOException;
import java.util.Enumeration;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import org.apache.xml.serialize.XMLSerializer;

import fr.inria.gemo.dataCorrob.graphGenerator.realDataGraphGenerator.searchEnginesGraphGenerator.SearchEngineManager;

/**
 * This class implements
 */
public abstract class SearchEngineExtractor {

	public abstract Enumeration<Answer> runQuery(String q);

	public void updateFromEngine() {
		try {
			System.out.print(SearchEngineManager.toString(this.getClass()));
			Document doc = this.openFile();
			Node nEngine = this.getEngineNode(doc);
			this.resetEngineNode(nEngine);
			Node nQueries = doc.getElementsByTagName("QueryData").item(0);
			NodeList nl = nQueries.getChildNodes();
			for(int i=0; i<nl.getLength();i++) {
				Node n = nl.item(i);
				if (n.getNodeType() == Node.ELEMENT_NODE) {
					if (n.getNodeName().equalsIgnoreCase("Query")) {
						String queryid = n.getAttributes().getNamedItem(
						"id").getNodeValue();
						Enumeration <Answer> answers = runQuery(n.getAttributes().getNamedItem(
						"name").getNodeValue());
						while (answers.hasMoreElements()) {
							Answer answer = answers.nextElement();
							Element newnode = doc.createElement("EngineAnswer");
							newnode.setAttribute("ref", queryid);
							newnode.setAttribute("answer", answer.answer);
							newnode.setAttribute("weight", answer.weight.toString());
							nEngine.appendChild(newnode);
						}
						System.out.print(".");
					}
				}
			}
			this.closeFile(doc);
			System.out.println("updated");
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private Document openFile() throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilder docbuilder = DocumentBuilderFactory.newInstance()
		.newDocumentBuilder();
		File f = new File(SearchEngineManager.file);
		return docbuilder.parse(f);
	}

	private Node getEngineNode(Document doc) {
		Node nEngine = null;
		NodeList nl = doc.getElementsByTagName("Engine");
		for(int i=0;i<nl.getLength();i++) {
			Node n = nl.item(i);
			String id = n.getAttributes().getNamedItem("id").getNodeValue();
			if(id.equals(SearchEngineManager.toString(this.getClass()))) {
				nEngine = n;
			}
		}
		if(nEngine==null) {
			Element newNode = doc.createElement("Engine");
			newNode.setAttribute("id",SearchEngineManager.toString(this.getClass()));
			doc.getElementsByTagName("EngineData").item(0).appendChild(newNode);
			nEngine = newNode;
		}
		return nEngine;
	}

	private void resetEngineNode(Node nEngine) {
		NodeList nlEngine = nEngine.getChildNodes();
		for(int i=nlEngine.getLength();i>0;i--) {
			nEngine.removeChild(nlEngine.item(i-1));
		}
	}

	private void closeFile(Document doc) throws IOException {
		XMLSerializer serializer = new XMLSerializer();
		serializer.setOutputCharStream(new java.io.FileWriter(SearchEngineManager.file));
		serializer.serialize(doc);
	}
}
