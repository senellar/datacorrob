/**
 * SearchEngineManager.java in dataCorrob
 * Copyright (C) by INRIA-Saclay, Alban Galland, 27 oct. 2008, All rights reserved
 */
package fr.inria.gemo.dataCorrob.graphGenerator.realDataGraphGenerator.searchEnginesGraphGenerator;

import fr.inria.gemo.dataCorrob.graphGenerator.realDataGraphGenerator.searchEnginesGraphGenerator.searchEngineExtractor.AlexaEngineExtractor;
import fr.inria.gemo.dataCorrob.graphGenerator.realDataGraphGenerator.searchEnginesGraphGenerator.searchEngineExtractor.AskEngineExtractor;
import fr.inria.gemo.dataCorrob.graphGenerator.realDataGraphGenerator.searchEnginesGraphGenerator.searchEngineExtractor.BaiduEngineExtractor;
import fr.inria.gemo.dataCorrob.graphGenerator.realDataGraphGenerator.searchEnginesGraphGenerator.searchEngineExtractor.CuilEngineExtractor;
import fr.inria.gemo.dataCorrob.graphGenerator.realDataGraphGenerator.searchEnginesGraphGenerator.searchEngineExtractor.ExaleadEngineExtractor;
import fr.inria.gemo.dataCorrob.graphGenerator.realDataGraphGenerator.searchEnginesGraphGenerator.searchEngineExtractor.GoogleEngineExtractor;
import fr.inria.gemo.dataCorrob.graphGenerator.realDataGraphGenerator.searchEnginesGraphGenerator.searchEngineExtractor.HakiaEngineExtractor;
import fr.inria.gemo.dataCorrob.graphGenerator.realDataGraphGenerator.searchEnginesGraphGenerator.searchEngineExtractor.LexxeEngineExtractor;
import fr.inria.gemo.dataCorrob.graphGenerator.realDataGraphGenerator.searchEnginesGraphGenerator.searchEngineExtractor.LiveEngineExtractor;
import fr.inria.gemo.dataCorrob.graphGenerator.realDataGraphGenerator.searchEnginesGraphGenerator.searchEngineExtractor.PowerSetEngineExtractor;
import fr.inria.gemo.dataCorrob.graphGenerator.realDataGraphGenerator.searchEnginesGraphGenerator.searchEngineExtractor.SearchEngineExtractor;
import fr.inria.gemo.dataCorrob.graphGenerator.realDataGraphGenerator.searchEnginesGraphGenerator.searchEngineExtractor.SogouEngineExtractor;
import fr.inria.gemo.dataCorrob.graphGenerator.realDataGraphGenerator.searchEnginesGraphGenerator.searchEngineExtractor.WikipediaEngineExtractor;
import fr.inria.gemo.dataCorrob.graphGenerator.realDataGraphGenerator.searchEnginesGraphGenerator.searchEngineExtractor.YahooEngineExtractor;

/**
 * This class implements 
 */
public abstract class SearchEngineManager {
	public static String file = "data/searchEnginesData.xml";

	public static Class<?> [] methods = {AlexaEngineExtractor.class,AskEngineExtractor.class,BaiduEngineExtractor.class,CuilEngineExtractor.class,ExaleadEngineExtractor.class,GoogleEngineExtractor.class,HakiaEngineExtractor.class,LexxeEngineExtractor.class,LiveEngineExtractor.class,PowerSetEngineExtractor.class,SogouEngineExtractor.class,WikipediaEngineExtractor.class,YahooEngineExtractor.class};
	
	public static Class<?> toClass(String type) {
		Class<?> typeEngineManager = null;
		if (type.equals("alexa")) {
			typeEngineManager = AlexaEngineExtractor.class;
		} else if (type.equals("ask")) {
			typeEngineManager = AskEngineExtractor.class;
		} else if (type.equals("baidu")) {
			typeEngineManager = BaiduEngineExtractor.class;
		} else if (type.equals("cuil")) {
			typeEngineManager = CuilEngineExtractor.class;
		} else if (type.equals("exalead")) {
			typeEngineManager = ExaleadEngineExtractor.class;
		} else if (type.equals("google")) {
			typeEngineManager = GoogleEngineExtractor.class;
		} else if (type.equals("hakia")) {
			typeEngineManager = HakiaEngineExtractor.class;
		} else if (type.equals("lexxe")) {
			typeEngineManager = LexxeEngineExtractor.class;
		} else if (type.equals("live")) {
			typeEngineManager = LiveEngineExtractor.class;
		} else if (type.equals("powerset")) {
			typeEngineManager = PowerSetEngineExtractor.class;
		} else if (type.equals("sogou")) {
			typeEngineManager = SogouEngineExtractor.class;
		} else if (type.equals("wikipedia")) {
			typeEngineManager = WikipediaEngineExtractor.class;
		} else if (type.equals("yahoo")) {
			typeEngineManager = YahooEngineExtractor.class;
		} else {
			System.err.println("Error (SearchEngineManager.java) : unknown SearchEngineManager type");
		}
		return typeEngineManager;
	}

	public static String toString(Class <?> type) {
		String typeEngine = null;
		if (AlexaEngineExtractor.class.equals(type)) {
			typeEngine="alexa";
		} else if (AskEngineExtractor.class.equals(type)) {
			typeEngine="ask";
		} else if (BaiduEngineExtractor.class.equals(type)) {
			typeEngine="baidu";
		} else if (CuilEngineExtractor.class.equals(type)) {
			typeEngine="cuil";
		} else if (ExaleadEngineExtractor.class.equals(type)) {
			typeEngine="exalead";
		} else if (GoogleEngineExtractor.class.equals(type)) {
			typeEngine="google";
		} else if (HakiaEngineExtractor.class.equals(type)) {
			typeEngine="hakia";
		} else if (LexxeEngineExtractor.class.equals(type)) {
			typeEngine="lexxe";
		} else if (LiveEngineExtractor.class.equals(type)) {
			typeEngine="live";
		} else if (PowerSetEngineExtractor.class.equals(type)) {
			typeEngine="powerset";
		} else if (SogouEngineExtractor.class.equals(type)) {
			typeEngine="sogou";
		} else if (WikipediaEngineExtractor.class.equals(type)) {
			typeEngine="wikipedia";
		} else if (YahooEngineExtractor.class.equals(type)) {
			typeEngine="yahoo";
		} else {
			System.err.println("Error (SearchEngineManager.java) : unknown SearchEngineManager type");
		}
		return typeEngine;
	}

	public static SearchEngineExtractor create(Class<?> type) {
		SearchEngineExtractor em= null;
		if (AlexaEngineExtractor.class.equals(type)) {
			em = new AlexaEngineExtractor();
		} else if (AskEngineExtractor.class.equals(type)) {
			em = new AskEngineExtractor();
		} else if (BaiduEngineExtractor.class.equals(type)) {
			em = new BaiduEngineExtractor();
		} else if (CuilEngineExtractor.class.equals(type)) {
			em = new CuilEngineExtractor();
		} else if (ExaleadEngineExtractor.class.equals(type)) {
			em = new ExaleadEngineExtractor();
		} else if (GoogleEngineExtractor.class.equals(type)) {
			em = new GoogleEngineExtractor();
		} else if (HakiaEngineExtractor.class.equals(type)) {
			em = new HakiaEngineExtractor();
		} else if (LexxeEngineExtractor.class.equals(type)) {
			em = new LexxeEngineExtractor();
		} else if (LiveEngineExtractor.class.equals(type)) {
			em = new LiveEngineExtractor();
		} else if (PowerSetEngineExtractor.class.equals(type)) {
			em = new PowerSetEngineExtractor();
		} else if (SogouEngineExtractor.class.equals(type)) {
			em = new SogouEngineExtractor();
		} else if (WikipediaEngineExtractor.class.equals(type)) {
			em = new WikipediaEngineExtractor();
		} else if (YahooEngineExtractor.class.equals(type)) {
			em = new YahooEngineExtractor();
		} else {
			System.err.println("Error (SearchEngineManager.java) : unknown Engine Manager type");
		}
		return em;
	}

	public static void update() {
		for(int i=0;i<SearchEngineManager.methods.length;i++) {
			SearchEngineExtractor em = SearchEngineManager.create(SearchEngineManager.methods[i]);
			em.updateFromEngine();
		}
	}

}
