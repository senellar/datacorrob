/**
 * 
 */
package fr.inria.gemo.dataCorrob.graphGenerator;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import fr.inria.gemo.dataCorrob.data.Fact;
import fr.inria.gemo.dataCorrob.data.Link;
import fr.inria.gemo.dataCorrob.data.Query;
import fr.inria.gemo.dataCorrob.data.Source;

/**
 * @author alban
 *
 */
public class XMLDataWriter {

	private Hashtable <String,Query> queries;

	private Hashtable <Integer,Fact> facts;

	private Hashtable <Integer,Source> sources;

	private Hashtable<Integer,Vector<Link>> links;

	public XMLDataWriter() {
		this.queries = new Hashtable <String,Query>();
		this.facts = new Hashtable <Integer,Fact>();
		this.sources = new Hashtable <Integer,Source>();
		this.links = new Hashtable <Integer,Vector<Link>>();
	}

	public void addQuery(Query q){
		this.queries.put(q.getId(), q);
	}

	public void addSource(Source s){
		this.sources.put(s.getId(), s);
	}

	public void addFact(Fact f){
		this.facts.put(f.getId(), f);
	}

	public void addLink(Link l){
		if(!this.links.containsKey(l.getFrom())) {
			this.links.put(l.getFrom(), new Vector<Link>());
		}
		this.links.get(l.getFrom()).add(l);
	}

	public void write(String file){
		try {
			PrintStream out = new PrintStream(new FileOutputStream(file, false));
			out.println("<RealData xsi:schemaLocation=\"http://gemofusion.gforge.inria.fr/xsd/datacorrob.xsd datacorrob.xsd\" xmlns=\"http://gemofusion.gforge.inria.fr/xsd/datacorrob.xsd\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">");
			out.println("<QueryData>");
			Enumeration <Query> qs = this.queries.elements();
			while(qs.hasMoreElements()) {
				Query q = qs.nextElement();
				out.println("<Query id=\""+q.getId()+"\" name=\""+q.getName()+"\" answer=\""+q.getAnswer()+"\"/>");
			}
			out.println("</QueryData>");
			out.println("<EngineData>");
			Enumeration <Source> ss = this.sources.elements();
			while(ss.hasMoreElements()) {
				Source s = ss.nextElement();
				out.println("<Engine id=\""+s.getId()+"\" name=\"s"+s.getId()+"\">");
				Enumeration <Link> ls = links.get(s.getId()).elements();
				while(ls.hasMoreElements()) {
					Link l = ls.nextElement();
					Fact f = this.facts.get(l.getTo());
					out.println("<EngineAnswer answer=\"f"+f.getId()+"\" ref=\""+f.getQueryId()+"\" weight=\""+l.getWeight()+"\"");
				}
				out.println("</Engine>");
			}
			out.println("</EngineData>");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

	}
}
