/**
 * PlotPrecisionRecallTest.java in dataCorrob (C) august 08 by agalland
 */
package fr.inria.gemo.dataCorrob.test.graphTest;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Vector;

import fr.inria.gemo.dataCorrob.arguments.Arguments;
import fr.inria.gemo.dataCorrob.dataManager.GraphManager;
import fr.inria.gemo.dataCorrob.method.Method;
import fr.inria.gemo.dataCorrob.test.ScoredFact;

/**
 * This class implements a test which plots the precision-recall test
 * @author agalland (Alban Galland) for Inria Saclay
 */
public class PlotPrecisionRecallTest extends GraphTest {
	/**
	 * This function is the standard constructor of the class.
	 * @param args the arguments of the program
	 */
	public PlotPrecisionRecallTest(Arguments args) {
		super(args,"Plots of the Precision-Recalls");
		this.sizePoint=1;
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.test.graphTest.GraphTest#plot(fr.inria.gemo.dataCorrob.dataManager.DataManager, fr.inria.gemo.dataCorrob.method.Method, java.lang.String, java.io.PrintStream)
	 */
	@Override
	protected void plot(GraphManager gm, Method m, String color) {
		ScoredFact[] facts = new ScoredFact[gm.getNbFacts()];
		int k = 0;
		double nbTotTrueFacts=0;
		Iterator<Integer> nodes = gm.getFacts();
		while (nodes.hasNext()) {
			Integer id = nodes.next();
			facts[k] = new ScoredFact(id, m.getScore(id));
			k++;
			if(gm.getIsTrue(id)) {
				nbTotTrueFacts++;
			}
		}
		Arrays.sort(facts);
		double nbTrueFacts = 0;
		Vector<Double> xv = new Vector<Double>();
		Vector<Double> yv = new Vector<Double>();
		for (int i = 0; i < gm.getNbFacts(); i++) {
			if (gm.getIsTrue(facts[i].id)) {
				nbTrueFacts++;
			} 
			double x=nbTrueFacts/nbTotTrueFacts;
			double y=nbTrueFacts/(i+1);
			if(this.args.point) {
				this.plotPoint(x, y, color);
			}
			xv.add(x);
			yv.add(y);
		}
		if(this.args.line) {
			double x=1;
			double y=nbTotTrueFacts/gm.getNbFacts();
			for(int i = xv.size();i>0;i--) {
				double yy=yv.elementAt(i-1);
				if (yy>y) {
					double xx= xv.elementAt(i-1);
					this.plotLine(x, xx, y, y, color);
					this.plotLine(xx, xx, y, yy, color);
					x = xx;
					y = yy;
				}
			}
			this.plotLine(x, 0, y, y, color);
			this.plotLine(0, 0, y, 1, color);
		}
	}

	/*
	 * (non-Javadoc)
	 * @see fr.inria.gemo.dataCorrob.test.GraphTest#printBox(java.io.PrintStream)
	 */
	@Override
	protected void printBox() {
		this.BeginPlot(0, 0, 1, 1);
		this.plotVerticalSide(0,0,1);
		this.plotVerticalSide(1,0,1);
		this.plotHorizontalSide(0,1,0);
		this.plotHorizontalSide(0,1,1);
		this.plotText(-0.01, -0.02, "0");
		this.plotText(1.01, -0.02, "recall");
		this.plotText(-0.01, 1.01, "precision");
		this.plotText(1.01, 1.01, "1");
	}
	
	
	@Override
	protected String getFile() {
		return "out/PrecRecall.xml";
	}
}
